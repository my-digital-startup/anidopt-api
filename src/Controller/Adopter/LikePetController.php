<?php

namespace App\Controller\Adopter;

use App\Entity\Pet\Pet;
use App\Entity\User\Adopter;
use App\Repository\Pet\PetRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LikePetController
{
    private PetRepository $petRepository;

    /**
     * Undocumented function
     *
     * @param PetRepository $petRepository
     */
    public function __construct(PetRepository $petRepository)
    {
        $this->petRepository = $petRepository;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function __invoke(Adopter $adopter, Request $request)
    {
        $data = $request->toArray();
        $favoritePets = $adopter->getFavoritePets();

        (!array_key_exists('petUuid', $data)) ? throw new HttpException(400) : '';

        $concernedPet = $this->getPet($data['petUuid']);
        (!$concernedPet) ? throw new HttpException(404) : '';

        if ($this->checkPetAlreadyLike($favoritePets, $concernedPet)) {
            $adopter->removeFavoritePet($concernedPet);
        } else {
            $adopter->addFavoritePet($concernedPet);
        }

        return $adopter;
    }

    /**
     * Get the concerned pet
     *
     * @param  string $uuid
     * @return Pet|null
     */
    private function getPet(string $uuid): ?Pet
    {
        return $this->petRepository->findOneBy(['uuid' => $uuid]) ?? null;
    }

    /**
     * Check if the pet is already like
     *
     * @param  Collection $pets
     * @param  Pet        $concernedPet
     * @return boolean
     */
    private function checkPetAlreadyLike(Collection $pets, Pet $concernedPet): bool
    {
        $flag = false;
        foreach ($pets as $pet) {
            $pet === $concernedPet ? $flag = true : '';
        }

        return $flag;
    }
}
