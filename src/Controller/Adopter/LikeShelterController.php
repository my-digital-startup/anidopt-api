<?php

namespace App\Controller\Adopter;

use App\Entity\User\Adopter;
use App\Entity\User\Shelter;
use App\Repository\User\ShelterRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LikeShelterController
{
    private ShelterRepository $shelterRepository;

    /**
     * Undocumented function
     *
     * @param ShelterRepository $shelterRepository
     */
    public function __construct(ShelterRepository $shelterRepository)
    {
        $this->shelterRepository = $shelterRepository;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function __invoke(Adopter $adopter, Request $request)
    {
        $data = $request->toArray();
        $likedShelters = $adopter->getLikedShelters();

        (!array_key_exists('shelterUuid', $data)) ? throw new HttpException(400) : '';

        $concernedShelter = $this->getShelter($data['shelterUuid']);
        (!$concernedShelter) ? throw new HttpException(404) : '';

        if ($this->checkShelterAlreadyLike($likedShelters, $concernedShelter)) {
            $adopter->removeLikedShelter($concernedShelter);
        } else {
            $adopter->addLikedShelter($concernedShelter);
        }

        return $adopter;
    }

    /**
     * Get the concerned shelter
     *
     * @param  string $uuid
     * @return Shelter|null
     */
    private function getShelter(string $uuid): ?Shelter
    {
        return $this->shelterRepository->findOneBy(['uuid' => $uuid]) ?? null;
    }

    /**
     * Check if the shelter is already like
     *
     * @param  Collection $shelters
     * @param  Shelter    $concernedShelter
     * @return boolean
     */
    private function checkShelterAlreadyLike(Collection $shelters, Shelter $concernedShelter): bool
    {
        $flag = false;
        foreach ($shelters as $shelter) {
            $shelter === $concernedShelter ? $flag = true : '';
        }

        return $flag;
    }
}
