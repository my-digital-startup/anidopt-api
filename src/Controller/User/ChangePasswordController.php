<?php

namespace App\Controller\User;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ChangePasswordController extends AbstractController
{
    private EntityManagerInterface $em;

    private UserPasswordEncoderInterface $userPasswordEncoder;

    /**
     * Undocumented function
     *
     * @param EntityManagerInterface $em
     * @param UserRepository         $userRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $userPasswordEncoder
    ) {
        $this->em = $em;
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    /**
     * Change password system
     *
     * @param Request $request
     */
    public function __invoke(Request $request): JsonResponse
    {
        try {
            $user = $this->getUser();
            $data = $request->toArray();

            /* Check if user exists */
            if (!$user) {
                return $this->json(
                    [
                    'detail' => 'L\'utilisateur n\'a pas été trouvé.'
                    ],
                    404
                );
            }

            if ($this->oldPasswordIsCorrect($user, $data['oldPassword'])) {
                $this->setNewPassword($user, $data['newPassword']);

                return $this->json(
                    [
                    'detail' => 'Le mot de passe a bien été modifié.'
                    ]
                );
            }

            return $this->json(
                [
                'detail' => 'Le mot de passe n\'a pas pu être modifié.'
                ],
                403
            );
        } catch (\Exception $e) {
            return $this->json(
                [
                'detail' => $e
                ],
                500
            );
        }
    }

    /**
     * Check if the old password is correct
     *
     * @param  Object $user
     * @param  string $oldPassword
     * @return boolean
     */
    private function oldPasswordIsCorrect($user, $oldPassword): bool
    {
        if ($this->userPasswordEncoder->isPasswordValid($user, $oldPassword)) {
            return true;
        }

        return false;
    }

    /**
     * Set the new password
     *
     * @param  User   $user
     * @param  string $newPassword
     * @return void
     */
    private function setNewPassword($user, $newPassword): void
    {
        $user->setPassword($newPassword);

        $this->em->persist($user);
        $this->em->flush();
    }
}
