<?php

namespace App\Controller\User;

// use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Repository\User\UserRepository;
use App\Service\EmailManager;
use CoopTilleuls\UrlSignerBundle\UrlSigner\UrlSignerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DeleteAccountConfirmController extends AbstractController
{
    private $emi;
    private $emailManager;
    private $urlSignerInterface;
    private $urlSigneruserRepositoryInterface;

    public function __construct(
        EntityManagerInterface $emi,
        EmailManager $emailManager,
        UrlSignerInterface $urlSignerInterface,
        UserRepository $userRepository
    ) {
        $this->emi = $emi;
        $this->emailManager = $emailManager;
        $this->urlSignerInterface = $urlSignerInterface;
        $this->userRepository = $userRepository;
    }

    /**
     * Undocumented function
     *
     * @param  Request $request
     * @return JsonResponse
     */
    public function __invoke(
        Request $request
    ) {

        if ((!$this->checkIsUrlValid($request)) or (!($this->findUser($request)))) {
            return $this->json(
                [
                'detail' => 'URL is not valid',
                ],
                400
            );
        }
        $this->findUser($request);
        $this->deleteUser($request);

        return $this->redirect($this->getParameter('REGISTER_ACTIVATION_PATH_SUCESSFULL'));
    }
    /**
     * Undocumented function
     *
     * @param  Request $request
     * @return void
     */
    public function checkIsUrlValid(Request $request)
    {
        $domainPath = $this->getParameter('DOMAIN_PATH');
        $requestUri = $request->getRequestUri();

        return $this->urlSignerInterface->validate($domainPath . $requestUri);
    }
    /**
     * Undocumented function
     *
     * @param  Request $request
     * @return void
     */
    public function findUser(Request $request)
    {
        $uuid = $request->attributes->all()['uuid'];
        $deletionToken = $request->attributes->all()['deletionToken'];

        $user = $this->userRepository->findOneBy(['uuid' => $uuid]);

        if ($user->getAccount()->getDeletionToken() === $deletionToken) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Undocumented function
     *
     * @param  Request $request
     * @return void
     */
    public function deleteUser(Request $request)
    {
        $uuid = $request->attributes->all()['uuid'];
        $user = $this->userRepository->findOneBy(['uuid' => $uuid]);

        $this->emailManager->sendEmail(
            'email/delete_account_confirm/index.html.twig',
            $user,
            [],
            'Anidopt <no-reply@anidopt.bzh>',
            'Confirmation de suppression de votre compte',
            null
        );

        $this->emi->remove($user);
        $this->emi->flush();

        return;
    }
}
