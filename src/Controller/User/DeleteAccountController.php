<?php

namespace App\Controller\User;

use App\Service\EmailManager;
// use CoopTilleuls\UrlSignerBundle\UrlSigner\UrlSignerInterface;
use CoopTilleuls\UrlSignerBundle\UrlSigner\UrlSignerInterface;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DeleteAccountController extends AbstractController
{
    // private PasswordEncoderInterface $passwordEncoderInterface;

    private $userPasswordEncoderInterface;
    private $emi;
    private $emailManager;
    private $urlSignerInterface;

    public function __construct(
        UserPasswordEncoderInterface $userPasswordEncoderInterface,
        EntityManagerInterface $emi,
        EmailManager $emailManager,
        UrlSignerInterface $urlSignerInterface
    ) {
        $this->userPasswordEncoderInterface = $userPasswordEncoderInterface;
        $this->emi = $emi;
        $this->emailManager = $emailManager;
        $this->urlSignerInterface = $urlSignerInterface;
    }

    /**
     * Undocumented function
     *
     * @param  Request $request
     * @return JsonResponse
     */
    public function __invoke(
        Request $request
    ): JsonResponse {
        date_default_timezone_set('Europe/Paris');
        // Check password
        if (!($this->checkPasswordValid($request->toArray()['password_confirm']))) {
            return $this->json(
                [
                'detail' => 'invalid password',
                ],
                403
            );
        }

        // Set deletion token and get it
        $deletionToken = $this->setUserDeletionToken();

        // Generate encoded url and get it
        $encodedDeletionUrl = $this->encodeDeletionUrl($deletionToken);

        // Get date
        $deletionDate = $this->setDeletionDate();

        // Send email with deletion date and deletion url (confirmation)
        $email = $this->sendEmail($deletionDate, $encodedDeletionUrl);

        return $this->json(
            [
            'detail' => 'Email successfull sended',
            ]
        );
    }

    /**
     * Undocumented function
     *
     * @param  String $password
     * @return void
     */
    public function checkPasswordValid(string $password)
    {
        return $this->userPasswordEncoderInterface->isPasswordValid($this->getUser(), $password);
    }

    public function setUserDeletionToken()
    {
        $deletionToken = md5(uniqid());
        $userAccount = $this->getUser()->getAccount();

        $userAccount->setDeletionToken($deletionToken);
        $this->emi->persist($userAccount);
        $this->emi->flush();

        return $deletionToken;
    }

    /**
     * Undocumented function
     *
     * @param  String $deletionToken
     * @return void
     */
    public function encodeDeletionUrl(string $deletionToken)
    {
        $domainPath = $this->getParameter('DOMAIN_PATH');
        $encodedUrl = $this->urlSignerInterface->sign(
            $domainPath .
                $this->generateUrl(
                    'users_account_deletion_confirm',
                    [
                        'uuid' => $this->getUser()->getUuid(),
                        'deletionToken' => $deletionToken,
                    ]
                )
        );
        return $encodedUrl;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function setDeletionDate()
    {
        $deletion_date = new DateTimeImmutable();
        $this->getUser()->setDeletedAt($deletion_date);

        $this->emi->persist($this->getUser());
        $this->emi->flush();

        return $deletion_date;
    }

    /**
     * Undocumented function
     *
     * @param  [type] $deletion_date
     * @param  [type] $encodedUrl
     * @return void
     */
    public function sendEmail($deletionDate, $encodedUrl)
    {
        $this->emailManager->sendEmail(
            'email/delete_account/index.html.twig',
            $this->getUser(),
            ['user_data' => [
                'deletion_date' => $deletionDate->format('d-m-Y H:i:s'),
                'encoded_url' => $encodedUrl,
            ]],
            'Anidopt <no-reply@anidopt.bzh>',
            'Confirmation of your account deletion',
            null
        );
    }
}
