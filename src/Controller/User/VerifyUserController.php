<?php

namespace App\Controller\User;

use App\Entity\User\User;
use App\Repository\AccountRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class VerifyUserController extends AbstractController
{
    private VerifyEmailHelperInterface $helper;

    private EntityManagerInterface $em;

    private AccountRepository $accountRepository;

    /**
     * Undocumented function
     *
     * @param VerifyEmailHelperInterface $helper
     * @param EntityManagerInterface     $em
     */
    public function __construct(
        VerifyEmailHelperInterface $helper,
        EntityManagerInterface $em,
        AccountRepository $accountRepository
    ) {
        $this->helper = $helper;
        $this->em = $em;
        $this->accountRepository = $accountRepository;
    }

    /**
     * Verify an user
     *
     * @param  Request $request
     * @param  User    $data
     * @return RedirectResponse
     */
    public function __invoke(Request $request): RedirectResponse
    {
        try {
            $user = $this->accountRepository->findOneBy(["activationToken" => $request->query->get('activationToken')])->getUser();
            $this->helper->validateEmailConfirmation(
                $request->getUri(),
                $user->getAccount()->getActivationToken(),
                $user->getEmail()
            );

            $user->getAccount()->setActivationToken(null);
            $user->getAccount()->setIsVerified(true);

            $this->em->persist($user);
            $this->em->flush();

            return $this->redirect($_ENV["REGISTER_ACTIVATION_PATH_SUCESSFULL"]);
        } catch (VerifyEmailExceptionInterface $e) {
            return $this->redirect($_ENV["REGISTER_ACTIVATION_PATH_UNSUCESSFULL"]);
        }
    }
}
