<?php

namespace App\Controller\User;

use App\Repository\User\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class ResetPasswordController extends AbstractController
{
    private EntityManagerInterface $em;

    private UserRepository $userRepository;

    /**
     * Undocumented function
     *
     * @param EntityManagerInterface $em
     * @param UserRepository         $userRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        UserRepository $userRepository
    ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
    }

    /**
     * Reset password system
     *
     * @param  Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        try {
            $data = $request->toArray();

            if (array_key_exists('email', $data) && count($data) === 1) {
                return $this->setResetPasswordToken($data);
            }

            if (
                array_key_exists('email', $data)
                && array_key_exists('token', $data)
                && array_key_exists('password', $data)
                && count($data) === 3
            ) {
                return $this->setNewPassword($data);
            }
        } catch (\Exception $e) {
            return $this->json(
                [
                'detail' => 'Une erreur est survenue.',
                'error' => $e
                ],
                500
            );
        }

        return $this->json(
            [
            'detail' => 'Une erreur est survenue.',
            ],
            500
        );
    }

    /**
     * First step of reset password system: We set up the token and send an email with a link containing the token and the email
     *
     * @param  array $data
     * @return JsonResponse
     */
    private function setResetPasswordToken($data): JsonResponse
    {
        try {
            $user = $this->userRepository->findOneBy(['email' => $data['email']]);
            if (!$user) {
                return $this->json(
                    [
                    'detail' => 'L\'utilisateur n\'a pas été trouvé.',
                    ],
                    404
                );
            }

            $user->getAccount()->setResetPasswordToken(md5(uniqid()));

            $this->em->persist($user);
            $this->em->flush();

            return $this->json(
                [
                'user' => [
                    'email' => $user->getEmail(),
                    'resetPasswordToken' => $user->getAccount()->getResetPasswordToken()
                ],
                'detail' => 'L\'email a bien été envoyé. Veuillez consulter votre messagerie pour poursuivre la démarche de réinitialisation de mot de passe.'
                ]
            );
        } catch (\Exception $e) {
            return $this->json(
                [
                'detail' => 'Une erreur est survenue.',
                'error' => $e
                ],
                500
            );
        }
    }

    /**
     * Second step of reset password system: Retrieve the new password as well as the token and email, verify the token and register the new password
     *
     * @param  array $data
     * @return void
     */
    private function setNewPassword($data): JsonResponse
    {
        try {
            $user = $this->userRepository->findOneBy(['email' => $data['email']]);
            if (!$user) {
                return $this->json(
                    [
                    'detail' => 'L\'utilisateur n\'a pas été trouvé.'
                    ],
                    404
                );
            }

            if ($user->getAccount()->getResetPasswordToken() !== $data['token']) {
                return $this->json(
                    [
                    'detail' => 'Vous ne pouvez pas accéder à cette ressource.'
                    ],
                    401
                );
            }

            $user->setPassword($data['password']);
            $user->getAccount()->setResetPasswordToken(null);

            $this->em->persist($user);
            $this->em->flush();

            return $this->json(
                [
                'detail' => 'Le mot de passe a bien été réinitialisé.'
                ],
                200
            );
        } catch (\Exception $e) {
            return $this->json(
                [
                'detail' => 'Une erreur est survenue.',
                'error' => $e
                ],
                500
            );
        }
    }
}
