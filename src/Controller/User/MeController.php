<?php

namespace App\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MeController extends AbstractController
{
    /**
     * Retrieve current user
     *
     * @return void
     */
    public function __invoke()
    {
        try {
            $user = $this->getUser();

            /* Check if user exists */
            if (!$user) {
                return $this->json(
                    [
                    'detail' => 'L\'utilisateur n\'a pas été trouvé.'
                    ],
                    404
                );
            }

            return $this->json(
                $user,
                200,
                [],
                [
                'groups' => [
                    'read:Adopter:item', 'read:Adopter:collection',
                    'read:Shelter:item', 'read:Shelter:collection'
                ]
                ]
            );
        } catch (\Exception $e) {
            $this->json(
                [
                'detail' => 'Une erreur est survenue.',
                ],
                500
            );
        }
    }
}
