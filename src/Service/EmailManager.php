<?php

namespace App\Service;

use App\Entity\User\User;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\MailerInterface;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EmailManager extends AbstractController
{
    private $mailerInterface;

    public function __construct(
        MailerInterface $mailerInterface
    ) {
        $this->mailerInterface = $mailerInterface;
    }

    /**
     * Email sender service
     *
     * @param  string      $emailTemplate
     * @param  array|null  $templateParams
     * @param  string|null $emailFrom
     * @param  string|null $emailSubject
     * @param  User        $user
     * @return void
     */
    public function sendEmail(
        string $emailTemplate,
        User $user,
        ?array $templateParams = null,
        ?string $emailFrom = 'Anidopt <no-reply@anidopt.bzh>',
        ?string $emailSubject = 'Email from Anidopt',
        ?string $attachementFromPath = null
    ) {
        $cssToInlineStyles = new CssToInlineStyles();

        $inlinedEmail = $cssToInlineStyles->convert(($this->render($emailTemplate, $templateParams)->getContent()));

        $email = new Email();
        $email->from($emailFrom);
        $email->to($user->getEmail());
        $email->subject($emailSubject);
        $email->html($inlinedEmail);
        if ($attachementFromPath) {
            $email->attachFromPath($attachementFromPath);
        }

        // dd($email);

        $this->mailerInterface->send($email);

        return;
    }
}
