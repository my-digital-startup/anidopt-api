<?php

namespace App\Entity;

use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\AccountRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AccountRepository::class)
 */
class Account
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    #[Assert\Type(
        type: 'string',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Assert\Length(
        min: 1,
        max: 500,
        minMessage: 'Le token d\'activation ne peut pas contenir moins de {{ limit }} caractères.',
        maxMessage: 'Le token d\'activation ne peut pas contenir plus de {{ limit }} caractères.',
    )]
    #[Groups(['read:Adopter:collection', 'read:Shelter:collection'])]
    private $activationToken;

    /**
     * @ORM\Column(type="boolean")
     */
    #[Groups(['read:Adopter:collection', 'read:Shelter:collection'])]
    #[Assert\Type(
        type: 'bool',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    private $isVerified;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    #[Assert\Type(
        type: 'string',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Assert\Length(
        min: 1,
        max: 500,
        minMessage: 'Le token de réintialisation de mot de passe ne peut pas contenir moins de {{ limit }} caractères.',
        maxMessage: 'Le token de réintialisation de mot de passe ne peut pas contenir plus de {{ limit }} caractères.',
    )]
    #[Groups(['read:Adopter:collection', 'read:Shelter:collection'])]
    private $resetPasswordToken;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    #[Assert\Type(
        type: 'string',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Assert\Length(
        max: 15,
        maxMessage: 'Le numéro de téléphone ne peut pas contenir plus de {{ limit }} caractères.',
    )]
    #[Groups(['read:Adopter:collection', 'read:Shelter:collection', 'patch:Adopter:item', 'patch:Shelter:item', 'read:Pet:item'])]
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Assert\Type(
        type: 'string',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Assert\Length(
        max: 255,
        maxMessage: 'L\'adresse ne peut pas contenir plus de {{ limit }} caractères.',
    )]
    #[Groups(['read:Adopter:collection', 'read:Shelter:collection', 'patch:Adopter:item', 'patch:Shelter:item', 'read:Pet:item'])]
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Assert\Type(
        type: 'string',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Assert\Length(
        max: 255,
        maxMessage: 'La ville ne peut pas contenir plus de {{ limit }} caractères.',
    )]
    #[Groups(['read:Adopter:collection', 'read:Shelter:collection', 'patch:Adopter:item', 'patch:Shelter:item','read:Pet:collection', 'read:Pet:item'])]
    private $city;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    #[Assert\Type(
        type: 'string',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Assert\Length(
        max: 6,
        maxMessage: 'L\'adresse ne peut pas contenir plus de {{ limit }} caractères.',
    )]
    #[Groups(['read:Adopter:collection', 'read:Shelter:collection', 'patch:Adopter:item', 'patch:Shelter:item', 'read:Pet:item'])]
    private $zipCode;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="account", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $deletionToken;

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getActivationToken(): ?string
    {
        return $this->activationToken;
    }

    /**
     * Undocumented function
     *
     * @param  string|null $activationToken
     * @return self
     */
    public function setActivationToken(?string $activationToken): self
    {
        $this->activationToken = $activationToken;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return boolean|null
     */
    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    /**
     * Undocumented function
     *
     * @param  boolean $isVerified
     * @return self
     */
    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getResetPasswordToken(): ?string
    {
        return $this->resetPasswordToken;
    }

    /**
     * Undocumented function
     *
     * @param  string|null $resetPasswordToken
     * @return self
     */
    public function setResetPasswordToken(?string $resetPasswordToken): self
    {
        $this->resetPasswordToken = $resetPasswordToken;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * Undocumented function
     *
     * @param  string|null $phone
     * @return self
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * Undocumented function
     *
     * @param  string|null $address
     * @return self
     */
    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * Undocumented function
     *
     * @param  string|null $city
     * @return self
     */
    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    /**
     * Undocumented function
     *
     * @param  string|null $zipCode
     * @return self
     */
    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * Undocumented function
     *
     * @param  User $user
     * @return self
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDeletionToken(): ?string
    {
        return $this->deletionToken;
    }

    public function setDeletionToken(?string $deletionToken): self
    {
        $this->deletionToken = $deletionToken;

        return $this;
    }
}
