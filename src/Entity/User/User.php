<?php

namespace App\Entity\User;

use App\Entity\Account;
use App\Entity\MediaObject;
use App\Entity\User\Adopter;
use App\Entity\User\Shelter;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\User\MeController;
use App\Repository\User\UserRepository;
use Gedmo\SoftDeleteable\SoftDeleteable;
use Doctrine\ORM\Mapping\EntityListeners;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\User\VerifyUserController;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Controller\User\DeleteAccountController;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\User\DeleteAccountConfirmController;
use Symfony\Component\Security\Core\User\UserInterface;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\EntityListeners({"App\EntityListener\UserListener"})
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discriminator",            type="string")
 * @ORM\DiscriminatorMap({
 *      "adopter"=Adopter::class,
 *      "shelter"=Shelter::class
 * })
 */
#[ApiResource(
    denormalizationContext: ['groups' => 'write:Shelter', 'write:Adopter'],
    collectionOperations: [
        'get' => [
            'normalization_context' => [
                'groups' => ['read:Adopter:collection', 'read:Shelter:collection']
            ]
        ],
        'verify' => [
            'method' => 'GET',
            'path' => '/users/verify',
            'controller' => VerifyUserController::class
        ],
        'me' => [
            'method' => 'GET',
            'path' => '/users/me',
            'controller' => MeController::class
        ]
    ],
    itemOperations: [
        'get' => [
            'normalization_context' => [
                'groups' => [
                    'read:Adopter:collection', 'read:Shelter:collection',
                    'read:Adopter:item', 'read:Shelter:item'
                ]
            ],
            'security' => 'object == user'
        ],
        'deleteAccount' => [
            'method' => 'PATCH',
            'path' => '/users/{uuid}/delete',
            'controller' => DeleteAccountController::class,
            // 'security' => 'object == user'
        ],
        'deleteAccountConfirm' => [
            'method' => 'GET',
            'path' => 'users/{uuid}/confirm/{deletionToken}',
            'controller' => DeleteAccountConfirmController::class
            // 'security' => 'object == user'
        ],
    ]
)]
#[UniqueEntity('email', 'Cet email est déjà utilisé au sein de l\'application.')]
abstract class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[Groups(['read:Adopter:collection', 'read:Shelter:collection'])]
    #[ApiProperty(identifier: false)]
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    #[Groups(
        [
        'read:Adopter:collection', 'read:Shelter:collection',
        'read:contact:collection', 'read:contact:item'
        ]
    )]
    #[ApiProperty(identifier: true)]
    private $uuid;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    #[Assert\NotBlank(
        message: 'Le champ email doit être renseigné.'
    )]
    #[Assert\Email(
        message: 'La valeur renseigné n\'est pas un email valide.'
    )]
    #[Assert\Type(
        type: 'string',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Assert\Length(
        min: 2,
        max: 180,
        minMessage: 'L\'email ne peut pas contenir moins de {{ limit }} caractères.',
        maxMessage: 'L\'email ne peut pas contenir plus de {{ limit }} caractères.',
    )]
    #[Groups(['read:Adopter:collection', 'read:Shelter:collection', 'write:Adopter', 'write:Shelter', 'write:Adopter:email', 'write:Shelter:email', 'read:Shelter:subresource'])]
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    #[Assert\NotNull(
        message: 'Le champ ne peut pas être vide.'
    )]
    #[Assert\Type(
        type: 'array',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Groups(['read:Adopter:collection', 'read:Shelter:collection'])]
    private $roles = [];

    /**
     * @var                       string The hashed password
     * @ORM\Column(type="string")
     */
    #[Assert\NotBlank(
        message: 'Le champ mot de passe doit être renseigné.'
    )]
    #[Assert\Type(
        type: 'string',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Assert\Length(
        min: 1,
        max: 255,
        minMessage: 'Le mot de passe ne peut pas contenir moins de {{ limit }} caractères.',
        maxMessage: 'Le mot de passe ne peut pas contenir plus de {{ limit }} caractères.',
    )]
    #[Groups(['write:Adopter', 'write:Shelter'])]
    private $password;

    /**
     * @ORM\Column(type="datetime")
     */
    #[Assert\Type(
        type: 'datetime',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Groups(['read:Adopter:collection', 'read:Shelter:collection'])]
    private $createdAt;

    /**
     * @ORM\OneToOne(targetEntity=Account::class, mappedBy="user", cascade={"persist", "remove"})
     */
    #[Groups(['read:Adopter:collection', 'read:Shelter:collection', 'put:Adopter:item', 'put:Shelter:item', 'read:Pet:collection', 'read:Pet:item'])]
    private $account;

    /**
     * @ORM\OneToMany(targetEntity=MediaObject::class, mappedBy="author", orphanRemoval=true, cascade={"remove"})
     */
    #[ApiSubresource()]
    private $mediaObjects;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    #[Groups(['read:Adopter:collection', 'read:Shelter:collection', 'read:Adopter:item', 'read:Shelter:item'])]
    private $deletedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->mediaObjects = new ArrayCollection();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Undocumented function
     *
     * @param  [type] $uuid
     * @return self
     */
    public function setUuid($uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Undocumented function
     *
     * @param  string $email
     * @return self
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * Undocumented function
     *
     * @param  array $roles
     * @return self
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Undocumented function
     *
     * @param  string $password
     * @return self
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * Undocumented function
     *
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param  \DateTimeInterface $createdAt
     * @return self
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        // set the owning side of the relation if necessary
        if ($account->getUser() !== $this) {
            $account->setUser($this);
        }

        $this->account = $account;

        return $this;
    }

    /**
     * @return Collection|MediaObject[]
     */
    public function getMediaObjects(): Collection
    {
        return $this->mediaObjects;
    }

    public function addMediaObject(MediaObject $mediaObject): self
    {
        if (!$this->mediaObjects->contains($mediaObject)) {
            $this->mediaObjects[] = $mediaObject;
            $mediaObject->setAuthor($this);
        }

        return $this;
    }

    public function removeMediaObject(MediaObject $mediaObject): self
    {
        if ($this->mediaObjects->removeElement($mediaObject)) {
            // set the owning side to null (unless already changed)
            if ($mediaObject->getAuthor() === $this) {
                $mediaObject->setAuthor(null);
            }
        }

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
