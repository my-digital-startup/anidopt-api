<?php

namespace App\Entity\User;

use App\Entity\Pet\Pet;
use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;
use App\EntityListener\ShelterListener;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Repository\User\ShelterRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\User\ResetPasswordController;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\User\ChangePasswordController;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=ShelterRepository::class)
 */
#[ApiResource(
    denormalizationContext: ['groups' => 'write:Shelter'],
    collectionOperations: [
        'get' => [
            'normalization_context' => [
                'groups' => ['read:Shelter:collection']
            ]
        ],
        'post'  => [
            'normalization_context' => [
                'groups' => ['read:Shelter:collection', 'read:Shelter:item']
            ],
        ],
        'reset_password' => [
            'method' => 'PUT',
            'path' => '/users/password/reset',
            'controller' => ResetPasswordController::class,
        ],
    ],
    itemOperations: [
        'get' => [
            'normalization_context' => [
                'groups' => ['read:Shelter:collection', 'read:Shelter:item']
            ]
        ],
        'patch' => [
            'normalization_context' => [
                'groups' => ['read:Shelter:collection', 'read:Shelter:item']
            ],
            'denormalization_context' => [
                'groups' => ['patch:Shelter:item']
            ],
            'security' => 'object == user'
        ],
        'email' => [
            'method' => 'PATCH',
            'path' => '/users/{uuid}/email',
            'normalization_context' => [
                'groups' => ['read:Shelter:collection', 'read:Shelter:item']
            ],
            'denormalization_context' => [
                'groups' => ['write:Shelter:email']
            ],
            'security' => 'object == user'
        ],
        'password' => [
            'method' => 'PATCH',
            'path' => '/users/{uuid}/password',
            'controller' => ChangePasswordController::class,
            'normalization_context' => [
                'groups' => ['read:Shelter:collection', 'read:Shelter:item']
            ]
        ]
    ],
    subresourceOperations: [
        'api_adopters_liked_shelters_get_subresource' => [
            'method' => 'GET',
            'normalization_context' => [
                'groups' => ['read:Shelter:subresource']
            ],
        ],
    ],
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'name' => 'partial',
        'account.zipCode' => 'exact',
        'account.city' => 'partial'
    ]
)]
#[UniqueEntity('name', 'Ce nom de refuge est déjà utilisé au sein de l\'application.')]
class Shelter extends User
{
    #[ApiProperty(identifier: false)]
    #[Groups(['read:Shelter:collection', 'read:Shelter:item'])]
    private $id;

    #[ApiProperty(identifier: true)]
    #[Groups(['read:Shelter:collection', 'read:Shelter:item', 'read:Shelter:subresource'])]
    private $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank(
        message: "Le champ nom ne peut pas être vide."
    )]
    #[Assert\Type(
        type: 'string',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Assert\Length(
        min: 2,
        max: 255,
        minMessage: 'Le nom ne peut pas contenir moins de {{ limit }} caractères.',
        maxMessage: 'Le nom ne peut pas contenir plus de {{ limit }} caractères.',
    )]
    #[Groups(
        [
        'read:Shelter:collection', 'read:Shelter:item', 'write:Shelter', 'patch:Shelter:item', 'read:Pet:collection', 'read:Shelter:subresource'
        ]
    )]
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\Type(
        type: 'string',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Assert\Length(
        min: 2,
        max: 255,
        minMessage: 'Le slug ne peut pas contenir moins de {{ limit }} caractères.',
        maxMessage: 'Le slug ne peut pas contenir plus de {{ limit }} caractères.',
    )]
    #[Groups(['read:Shelter:collection', 'read:Shelter:item', 'read:Pet:collection', 'read:Shelter:subresource'])]
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity=Pet::class, mappedBy="shelter", orphanRemoval=true)
     */
    #[Groups(['read:Shelter:item'])]
    private $pets;

    /**
     * @ORM\ManyToMany(targetEntity=Adopter::class, mappedBy="likedShelters")
     */
    private $adoptersWhoHaveLiked;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        parent::__construct();
        $this->pets = new ArrayCollection();
        $this->adoptersWhoHaveLiked = new ArrayCollection();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Undocumented function
     *
     * @param  string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Undocumented function
     *
     * @param  string $slug
     * @return self
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|Pet[]
     */
    public function getPets(): Collection
    {
        return $this->pets;
    }

    public function addPet(Pet $pet): self
    {
        if (!$this->pets->contains($pet)) {
            $this->pets[] = $pet;
            $pet->setShelter($this);
        }

        return $this;
    }

    public function removePet(Pet $pet): self
    {
        if ($this->pets->removeElement($pet)) {
            // set the owning side to null (unless already changed)
            if ($pet->getShelter() === $this) {
                $pet->setShelter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Adopter[]
     */
    public function getAdoptersWhoHaveLiked(): Collection
    {
        return $this->adoptersWhoHaveLiked;
    }

    public function addAdoptersWhoHaveLiked(Adopter $adoptersWhoHaveLiked): self
    {
        if (!$this->adoptersWhoHaveLiked->contains($adoptersWhoHaveLiked)) {
            $this->adoptersWhoHaveLiked[] = $adoptersWhoHaveLiked;
            $adoptersWhoHaveLiked->addLikedShelter($this);
        }

        return $this;
    }

    public function removeAdoptersWhoHaveLiked(Adopter $adoptersWhoHaveLiked): self
    {
        if ($this->adoptersWhoHaveLiked->removeElement($adoptersWhoHaveLiked)) {
            $adoptersWhoHaveLiked->removeLikedShelter($this);
        }

        return $this;
    }
}
