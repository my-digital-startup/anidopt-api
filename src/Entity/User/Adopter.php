<?php

namespace App\Entity\User;

use App\Entity\Pet\Pet;
use App\Entity\User\User;
use App\Entity\Pet\Contact;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\User\AdopterRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Adopter\LikePetController;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Controller\User\ResetPasswordController;
use Doctrine\Common\Collections\ArrayCollection;
use App\Controller\Adopter\LikeShelterController;
use App\Controller\User\ChangePasswordController;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AdopterRepository::class)
 */
#[ApiResource(
    denormalizationContext: ['groups' => 'write:Adopter'],
    collectionOperations: [
        'get' => [
            'normalization_context' => [
                'groups' => ['read:Adopter:collection']
            ]
        ],
        'post'  => [
            'normalization_context' => [
                'groups' => ['read:Adopter:collection', 'read:Adopter:item']
            ],
        ],
        'reset_password' => [
            'method' => 'PUT',
            'path' => '/users/password/reset',
            'controller' => ResetPasswordController::class,
        ]
    ],
    itemOperations: [
        'get' => [
            'normalization_context' => [
                'groups' => ['read:Adopter:collection', 'read:Adopter:item']
            ],
            'security' => 'object == user'
        ],
        'patch' => [
            'normalization_context' => [
                'groups' => ['read:Adopter:collection', 'read:Adopter:item']
            ],
            'denormalization_context' => [
                'groups' => ['patch:Adopter:item']
            ],
            'security' => 'object == user'
        ],
        'email' => [
            'method' => 'PATCH',
            'path' => '/users/{uuid}/email',
            'normalization_context' => [
                'groups' => ['read:Adopter:collection', 'read:Adopter:item']
            ],
            'denormalization_context' => [
                'groups' => ['write:Adopter:email']
            ],
            'security' => 'object == user'
        ],
        'password' => [
            'method' => 'PATCH',
            'path' => '/users/{uuid}/password',
            'controller' => ChangePasswordController::class,
            'normalization_context' => [
                'groups' => ['read:Adopter:collection', 'read:Adopter:item']
            ]
        ],
        'pet' => [
            'method' => 'PATCH',
            'path' => '/adopters/{uuid}/pets',
            'controller' => LikePetController::class,
            'normalization_context' => [
                'groups' => ['read:Adopter:collection', 'read:Adopter:item']
            ],

        ],
        'liked_shelters' => [
            'method' => 'PATCH',
            'path' => '/adopters/{uuid}/shelters',
            'controller' => LikeShelterController::class,
            'normalization_context' => [
                'groups' => ['read:Adopter:collection', 'read:Adopter:item']
            ],
            'security' => 'object == user'
        ]
    ],
    subresourceOperations: [
        'favorite_pets_get_subresource' => [
            'path' => '/adopters/{uuid}/pets'
        ],
        'liked_shelters_get_subresource' => [
            'path' => '/adopters/{uuid}/shelters'
        ],
    ],
)]
class Adopter extends User
{
    #[ApiProperty(identifier: false)]
    #[Groups(['read:Adopter:collection', 'read:Adopter:item'])]
    private $id;

    #[ApiProperty(identifier: true)]
    #[Groups(['read:Adopter:collection', 'read:Adopter:item'])]
    private $uuid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Assert\Type(
        type: 'string',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Assert\Length(
        max: 255,
        maxMessage: 'Le prénom ne peut pas contenir plus de {{ limit }} caractères.',
    )]
    #[Groups(['read:Adopter:collection', 'read:Adopter:item', 'patch:Adopter:item'])]
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Assert\Type(
        type: 'string',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Assert\Length(
        max: 255,
        maxMessage: 'Le nom ne peut pas contenir plus de {{ limit }} caractères.',
    )]
    #[Groups(['read:Adopter:collection', 'read:Adopter:item', 'patch:Adopter:item'])]
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Assert\Type(
        type: 'string',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Assert\Length(
        max: 255,
        maxMessage: 'Le pseudo ne peut pas contenir plus de {{ limit }} caractères.',
    )]
    #[Groups(['read:Adopter:collection', 'read:Adopter:item', 'patch:Adopter:item'])]
    private $pseudo;

    /**
     * @ORM\ManyToMany(targetEntity=Pet::class)
     */
    #[ApiSubresource()]
    private $favoritePets;

    /**
     * @ORM\OneToMany(targetEntity=Contact::class, mappedBy="adopter")
     */
    #[ApiSubresource()]
    private $contacts;

    /**
     * @ORM\ManyToMany(targetEntity=Shelter::class, inversedBy="adoptersWhoHaveLiked")
     */
    #[ApiSubresource()]
    private $likedShelters;

    public function __construct()
    {
        parent::__construct();
        $this->favoritePets = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->likedShelters = new ArrayCollection();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * Undocumented function
     *
     * @param  string|null $firstName
     * @return self
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }
    /**
     * Undocumented function
     *
     * @param  string $lastName
     * @return self
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    /**
     * Undocumented function
     *
     * @param  string|null $pseudo
     * @return self
     */
    public function setPseudo(?string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Collection
     */
    public function getFavoritePets(): Collection
    {
        return $this->favoritePets;
    }

    /**
     * Undocumented function
     *
     * @param  Pet $favoritePet
     * @return self
     */
    public function addFavoritePet(Pet $favoritePet): self
    {
        if (!$this->favoritePets->contains($favoritePet)) {
            $this->favoritePets[] = $favoritePet;
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param  Pet $favoritePet
     * @return self
     */
    public function removeFavoritePet(Pet $favoritePet): self
    {
        $this->favoritePets->removeElement($favoritePet);

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setAdopter($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getAdopter() === $this) {
                $contact->setAdopter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Shelter[]
     */
    public function getLikedShelters(): Collection
    {
        return $this->likedShelters;
    }

    public function addLikedShelter(Shelter $likedShelter): self
    {
        if (!$this->likedShelters->contains($likedShelter)) {
            $this->likedShelters[] = $likedShelter;
        }

        return $this;
    }

    public function removeLikedShelter(Shelter $likedShelter): self
    {
        $this->likedShelters->removeElement($likedShelter);

        return $this;
    }
}
