<?php

namespace App\Entity\Pet;

use Ramsey\Uuid\Uuid;
use DateTimeImmutable;
use App\Entity\Pet\Pet;
use App\Entity\User\Adopter;
use Symfony\Component\Uid\Ulid;
use Doctrine\ORM\Mapping as ORM;
use App\EntityListener\ContactListener;
use App\Repository\Pet\ContactRepository;
use Doctrine\ORM\Mapping\EntityListeners;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Contact\MyContactsController;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ContactRepository::class)
 * @ORM\EntityListeners({"App\EntityListener\ContactListener"})
 */

#[ApiResource(
    normalizationContext:['groups' => 'read:contact:item'],
    collectionOperations:[
        'post' => [
            'denormalization_context' => [
                'groups' => ['write:contact:collection'],
            ],
        ],
        'get' => [
            'normalization_context' => [
                'groups' => ['read:contact:collection']
            ],
        ],
    ],
    itemOperations:[
        'get' => [
            'normalization_context' => [
                'groups' => ['read:contact:item']
            ],
            'path' => '/contacts/{uuid}',
        ],
    ],
)]
class Contact
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[ApiProperty(identifier:false)]
    #[Groups(
        [
        'read:contact:collection',
        'write:contact:collection'
        ]
    )]
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[ApiProperty(identifier:true)]
    #[Groups(
        ['read:contact:collection',
        'write:contact:collection','read:contact:item'
        ]
    )]
    private $uuid;

    /**
     * @ORM\ManyToOne(targetEntity=Adopter::class, inversedBy="contacts")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Groups(
        ['read:contact:collection', 'read:contact:item',
        'write:contact:collection','read:contact:item'
        ]
    )]
    private $adopter;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Groups(
        ['read:contact:collection','read:contact:item'
        ]
    )]
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Pet::class, inversedBy="contacts")
     */
    #[Groups(
        ['write:contact:collection', 'read:contact:collection', 'read:contact:item',
        'write:contact:collection','read:contact:item'
        ]
    )]
    private $pet;



    public function __construct()
    {
        $this->pet = new ArrayCollection();
        $this->uuid = strval(new Ulid());
        $this->createdAt = new DateTimeImmutable();
        $this->isRead = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getAdopter(): ?Adopter
    {
        return $this->adopter;
    }

    public function setAdopter(?Adopter $adopter): self
    {
        $this->adopter = $adopter;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPet(): ?Pet
    {
        return $this->pet;
    }

    public function setPet(?Pet $pet): self
    {
        $this->pet = $pet;

        return $this;
    }
}
