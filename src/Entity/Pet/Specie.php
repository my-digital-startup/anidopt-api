<?php

namespace App\Entity\Pet;

use DateTimeImmutable;
use App\Entity\Pet\Pet;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\Pet\SpecieRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=SpecieRepository::class)
 */
#[ApiResource(
    denormalizationContext: ['groups' => 'write:Specie'],
    normalizationContext: ['groups' => 'read:Specie'],
    collectionOperations: ['get', 'post'],
    itemOperations: ['get', 'patch', 'delete'],
)]
#[UniqueEntity('name')]
class Specie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[Groups(['read:Specie', 'read:Pet:collection', 'read:Shelter:item'])]
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(max: 255)]
    #[Groups(['read:Specie','read:Pet:collection', 'read:Shelter:item', 'write:Specie'])]
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Pet::class, mappedBy="specie")
     */
    private Collection $pets;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Groups(['read:Specie'])]
    private DateTimeImmutable $createdAt;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->pets = new ArrayCollection();
        $this->createdAt = new DateTimeImmutable();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Undocumented function
     *
     * @param  string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Collection|Pet[]
     */
    public function getPets(): Collection
    {
        return $this->pets;
    }

    /**
     * Undocumented function
     *
     * @param  Pet $pet
     * @return self
     */
    public function addPet(Pet $pet): self
    {
        if (!$this->pets->contains($pet)) {
            $this->pets[] = $pet;
            $pet->setSpecie($this);
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param  Pet $pet
     * @return self
     */
    public function removePet(Pet $pet): self
    {
        if ($this->pets->removeElement($pet)) {
            if ($pet->getSpecie() === $this) {
                $pet->setSpecie(null);
            }
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return DateTimeImmutable|null
     */
    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param  DateTimeImmutable $createdAt
     * @return self
     */
    public function setCreatedAt(DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
