<?php

namespace App\Entity\Pet;

use DateTimeImmutable;
use App\Entity\Pet\Tag;
use App\Entity\Pet\Breed;
use App\Entity\Pet\Specie;
use App\Entity\MediaObject;
use App\Entity\User\Shelter;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\Pet\PetRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\EntityListeners({"App\EntityListener\PetListener"})
 * @ORM\Entity(repositoryClass=PetRepository::class)
 */
#[UniqueEntity('slug')]
#[ApiResource(
    denormalizationContext: ['groups' => 'write:Pet'],
    collectionOperations: [
        'get' => [
            'normalization_context' => [
                'groups' => ['read:Pet:collection']
            ]
        ],
        'post' => [
            'normalization_context' => [
                'groups' => ['read:Pet:collection', 'read:Pet:item']
            ]
        ],
    ],
    itemOperations: [
        'get' => [
            'normalization_context' => [
                'groups' => ['read:Pet:collection', 'read:Pet:item']
            ],
        ],
        'patch' => [
            'normalization_context' => [
                'groups' => ['read:Pet:collection', 'read:Pet:item']
            ],
            'denormalization_context' => [
                'groups' => ['patch:Pet']
            ],
            'security' => 'object.shelter == user'
        ],
        'delete' => ['security' => 'object.shelter == user']
    ],
    subresourceOperations: [
        'api_adopters_favorite_pets_get_subresource' => [
            'method' => 'GET',
            'normalization_context' => [
                'groups' => ['read:Pet:subresource']
            ],
        ],
    ],
)]
#[ApiFilter(DateFilter::class, properties: ['birthday'])]
#[ApiFilter(
    BooleanFilter::class,
    properties: ['isMale', 'isSterilised', 'isSos'],
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'specie.name' => 'exact',
        'breeds.name' => 'exact',
        'tag.name' => 'exact'
    ]
)]
class Pet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[ApiProperty(identifier: false)]
    #[Groups(['read:Pet:item', 'read:Shelter:item', 'read:Pet:subresource'])]
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    #[ApiProperty(identifier: true)]
    #[Groups(
        [
        'read:Pet:collection', 'read:Shelter:item', 'read:Pet:subresource',
        'read:contact:item', 'read:contact:collection'
        ]
    )]
    private string $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(max: 255)]
    #[Groups(['read:Pet:collection', 'read:Shelter:item', 'write:Pet', 'patch:Pet', 'read:Pet:subresource'])]
    private string $name;

    /**
     * @ORM\Column(type="string", length=500, unique=true)
     */
    #[Assert\Length(max: 500)]
    #[Groups(['read:Pet:collection', 'read:Shelter:item', 'read:Pet:subresource'])]
    private ?string $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    #[Groups(['read:Pet:item', 'write:Pet', 'patch:Pet', 'read:Adopter:pets', 'read:Pet:subresource'])]
    private ?string $description;

    /**
     * @ORM\Column(type="boolean")
     */
    #[Assert\NotNull()]
    #[Groups(['read:Pet:collection', 'read:Shelter:item', 'write:Pet', 'patch:Pet', 'read:Pet:subresource'])]
    private bool $isMale;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    #[Groups(['read:Pet:collection', 'read:Shelter:item', 'write:Pet', 'patch:Pet', 'read:Pet:subresource'])]
    private ?DateTimeImmutable $birthday;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    #[Groups(['read:Pet:item', 'write:Pet', 'patch:Pet', 'read:Pet:subresource'])]
    private bool $isSterilised;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Assert\Length(max: 255)]
    #[Groups(['read:Pet:collection', 'read:Shelter:item', 'write:Pet', 'patch:Pet', 'read:Pet:subresource'])]
    private ?string $identificationNumber;

    /**
     * @ORM\Column(type="boolean")
     */
    #[Assert\NotNull()]
    #[Groups(['read:Pet:collection', 'read:Shelter:item', 'write:Pet', 'patch:Pet', 'read:Pet:subresource'])]
    private $isSos;

    /**
     * @ORM\ManyToOne(targetEntity=Shelter::class, inversedBy="pets")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Assert\NotNull()]
    #[Groups(['read:Pet:collection', 'read:Pet:item', 'write:Pet', 'read:Pet:subresource'])]
    public Shelter $shelter;

    /**
     * @ORM\ManyToOne(targetEntity=Specie::class, inversedBy="pets")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Assert\NotNull()]
    #[Groups(['read:Pet:collection', 'read:Shelter:item', 'write:Pet', 'patch:Pet', 'read:Pet:subresource'])]
    private Specie $specie;

    /**
     * @ORM\ManyToMany(targetEntity=Breed::class, inversedBy="pets")
     */
    #[Groups(['read:Pet:collection', 'read:Shelter:item', 'write:Pet', 'patch:Pet', 'read:Pet:subresource'])]
    private Collection $breeds;

    /**
     * @ORM\ManyToMany(targetEntity=Tag::class, inversedBy="pets")
     */
    #[Groups(['read:Pet:item', 'write:Pet', 'patch:Pet', 'read:Pet:subresource'])]
    private Collection $tags;

    /**
     * @ORM\ManyToOne(targetEntity=MediaObject::class)
     * @ORM\JoinColumn(nullable=true)
     */
    #[Groups(['read:Pet:collection', 'write:Pet', 'patch:Pet', 'read:Pet:subresource'])]
    private ?MediaObject $mainImage = null;

    /**
     * @ORM\OneToMany(targetEntity=MediaObject::class, mappedBy="pet")
     */
    #[Groups(['read:Pet:collection', 'write:Pet', 'patch:Pet', 'read:Pet:subresource'])]
    private ?Collection $images;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Groups(['read:Pet:collection', 'read:Shelter:item', 'read:Pet:subresource'])]
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=Contact::class, mappedBy="pet")
     */
    private $contacts;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->breeds = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->createdAt = new DateTimeImmutable();
        $this->images = new ArrayCollection();
        $this->contacts = new ArrayCollection();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    /**
     * Undocumented function
     *
     * @param  string $uuid
     * @return self
     */
    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Undocumented function
     *
     * @param  string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Undocumented function
     *
     * @param  string $slug
     * @return self
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Undocumented function
     *
     * @param  string|null $description
     * @return self
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return boolean|null
     */
    public function getIsMale(): ?bool
    {
        return $this->isMale;
    }

    /**
     * Undocumented function
     *
     * @param  boolean $isMale
     * @return self
     */
    public function setIsMale(bool $isMale): self
    {
        $this->isMale = $isMale;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return DateTimeImmutable|null
     */
    public function getBirthday(): ?DateTimeImmutable
    {
        return $this->birthday;
    }

    /**
     * Undocumented function
     *
     * @param  DateTimeImmutable|null $birthday
     * @return self
     */
    public function setBirthday(?DateTimeImmutable $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return boolean|null
     */
    public function getIsSterilised(): ?bool
    {
        return $this->isSterilised;
    }

    /**
     * Undocumented function
     *
     * @param  boolean|null $isSterilised
     * @return self
     */
    public function setIsSterilised(?bool $isSterilised): self
    {
        $this->isSterilised = $isSterilised;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getIdentificationNumber(): ?string
    {
        return $this->identificationNumber;
    }

    /**
     * Undocumented function
     *
     * @param  string|null $identificationNumber
     * @return self
     */
    public function setIdentificationNumber(?string $identificationNumber): self
    {
        $this->identificationNumber = $identificationNumber;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Shelter|null
     */
    public function getShelter(): ?Shelter
    {
        return $this->shelter;
    }

    /**
     * Undocumented function
     *
     * @param  Shelter|null $shelter
     * @return self
     */
    public function setShelter(?Shelter $shelter): self
    {
        $this->shelter = $shelter;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Specie|null
     */
    public function getSpecie(): ?Specie
    {
        return $this->specie;
    }

    /**
     * Undocumented function
     *
     * @param  Specie|null $specie
     * @return self
     */
    public function setSpecie(?Specie $specie): self
    {
        $this->specie = $specie;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Collection|Breed[]
     */
    public function getBreeds(): Collection
    {
        return $this->breeds;
    }

    /**
     * Undocumented function
     *
     * @param  Breed $breed
     * @return self
     */
    public function addBreed(Breed $breed): self
    {
        if (!$this->breeds->contains($breed)) {
            $this->breeds[] = $breed;
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param  Breed $breed
     * @return self
     */
    public function removeBreed(Breed $breed): self
    {
        $this->breeds->removeElement($breed);

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    /**
     * Undocumented function
     *
     * @param  Tag $tag
     * @return self
     */
    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param  Tag $tag
     * @return self
     */
    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return MediaObject|null
     */
    public function getMainImage(): ?MediaObject
    {
        return $this->mainImage;
    }

    /**
     * Undocumented function
     *
     * @param  MediaObject|null $mainImage
     * @return self
     */
    public function setMainImage(?MediaObject $mainImage): self
    {
        $this->mainImage = $mainImage;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return DateTimeImmutable|null
     */
    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param  DateTimeImmutable $createdAt
     * @return self
     */
    public function setCreatedAt(DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|MediaObject[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(MediaObject $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setPet($this);
        }

        return $this;
    }

    public function removeImage(MediaObject $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getPet() === $this) {
                $image->setPet(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setPet($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getPet() === $this) {
                $contact->setPet(null);
            }
        }

        return $this;
    }

    public function getIsSos(): ?bool
    {
        return $this->isSos;
    }

    public function setIsSos(bool $isSos): self
    {
        $this->isSos = $isSos;

        return $this;
    }
}
