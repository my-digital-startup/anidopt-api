<?php

namespace App\Entity\Pet;

use DateTimeImmutable;
use App\Entity\Pet\Pet;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\Pet\BreedRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=BreedRepository::class)
 */
#[ApiResource(
    denormalizationContext: ['groups' => 'write:Breed'],
    normalizationContext: ['groups' => 'read:Breed'],
    collectionOperations: ['get', 'post'],
    itemOperations: ['get', 'patch', 'delete'],
)]
#[UniqueEntity('name')]
class Breed
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[Groups(['read:Breed','read:Pet:collection', 'read:Shelter:item'])]
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(max: 255)]
    #[Groups(['read:Breed','read:Pet:collection', 'read:Shelter:item', 'write:Breed'])]
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Pet::class, mappedBy="breeds")
     */
    private Collection $pets;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Groups(['read:Breed'])]
    private DateTimeImmutable $createdAt;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->pets = new ArrayCollection();
        $this->createdAt = new DateTimeImmutable();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Undocumented function
     *
     * @param  string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Collection|Pet[]
     */
    public function getPets(): Collection
    {
        return $this->pets;
    }

    /**
     * Undocumented function
     *
     * @param  Pet $pet
     * @return self
     */
    public function addPet(Pet $pet): self
    {
        if (!$this->pets->contains($pet)) {
            $this->pets[] = $pet;
            $pet->addBreed($this);
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param  Pet $pet
     * @return self
     */
    public function removePet(Pet $pet): self
    {
        if ($this->pets->removeElement($pet)) {
            $pet->removeBreed($this);
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return DateTimeImmutable|null
     */
    public function getcreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param  DateTimeImmutable $createdAt
     * @return self
     */
    public function setcreatedAt(DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
