<?php

namespace App\Entity\Pet;

use DateTimeImmutable;
use App\Entity\Pet\Pet;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\Pet\TagRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=TagRepository::class)
 */
#[ApiResource(
    denormalizationContext: ['groups' => 'write:Tag'],
    normalizationContext: ['groups' => 'read:Tag'],
    collectionOperations: ['get', 'post'],
    itemOperations: ['get', 'patch', 'delete'],
)]
#[UniqueEntity('name')]
class Tag
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[Assert\Type(
        type: 'integer',
        message: 'La valeur renseigné n\'est pas une valeur de type {{ type }}.',
    )]
    #[Groups(['read:Tag','read:Pet:item'])]
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    #[Assert\NotBlank()]
    #[Assert\Length(max: 255)]
    #[Groups(['read:Tag','read:Pet:collection', 'read:Shelter:item', 'write:Tag'])]
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Pet::class, mappedBy="tags")
     */
    private Collection $pets;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Groups(['read:Tag'])]
    private DateTimeImmutable $createdAt;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->pets = new ArrayCollection();
        $this->createdAt = new DateTimeImmutable();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Undocumented function
     *
     * @param  string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Collection|Pet[]
     */
    public function getPets(): Collection
    {
        return $this->pets;
    }

    /**
     * Undocumented function
     *
     * @param  Pet $pet
     * @return self
     */
    public function addPet(Pet $pet): self
    {
        if (!$this->pets->contains($pet)) {
            $this->pets[] = $pet;
            $pet->addTag($this);
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param  Pet $pet
     * @return self
     */
    public function removePet(Pet $pet): self
    {
        if ($this->pets->removeElement($pet)) {
            $pet->removeTag($this);
        }

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return DateTimeImmutable|null
     */
    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Undocumented function
     *
     * @param  DateTimeImmutable $createdAt
     * @return self
     */
    public function setCreatedAt(DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
