<?php

// api/src/Entity/MediaObject.php
namespace App\Entity;

use App\Entity\Pet\Pet;
use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\PostMediaObjectController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
#[ApiResource(
    normalizationContext: ['groups' => ['read:MediaObject']],
    itemOperations: [
        'get',
        'delete' => ['security' => 'object.author == user']
    ],
    collectionOperations: [
        'get',
        'post' => [
            'controller' => PostMediaObjectController::class,
            'deserialize' => false,
            'validation_groups' => ['Default', 'write:MediaObject'],
        ],
    ]
)]
class MediaObject
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @ORM\Id
     */
    private ?int $id = null;

    #[ApiProperty(iri: 'http://schema.org/contentUrl')]
    #[Groups(['read:MediaObject', 'read:Pet:collection'])]
    public ?string $contentUrl = null;

    /**
     * @Vich\UploadableField(mapping="media_object", fileNameProperty="filePath")
     */
    #[Assert\NotNull(groups: ['write:MediaObject'])]
    #[Assert\File(maxSize: "2M")]
    public ?File $file = null;

    /**
     * @ORM\Column(nullable=true)
     */
    public ?string $filePath = null;

    /**
     * @ORM\ManyToOne(targetEntity=Pet::class, inversedBy="images")
     */
    #[Groups(['read:MediaObject'])]
    private ?Pet $pet;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="mediaObjects")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Groups(['read:MediaObject'])]
    public User $author;

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return Pet|null
     */
    public function getPet(): ?Pet
    {
        return $this->pet;
    }

    /**
     * Undocumented function
     *
     * @param  Pet|null $pet
     * @return self
     */
    public function setPet(?Pet $pet): self
    {
        $this->pet = $pet;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return User|null
     */
    public function getAuthor(): ?User
    {
        return $this->author;
    }

    /**
     * Undocumented function
     *
     * @param  User|null $author
     * @return self
     */
    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param  ExecutionContextInterface $context
     * @return void
     */
    #[Assert\Callback()]
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (
            ! in_array(
                $this->file->getMimeType(),
                array(
                'image/jpeg',
                'image/jpg',
                'image/png',
                'image/webp'
                )
            )
        ) {
            $context
                ->buildViolation('Seules les extensions suivantes sont autorisées (.jpeg, .jpg, .png, .webp)')
                ->atPath('filePath')
                ->addViolation();
        }
    }
}
