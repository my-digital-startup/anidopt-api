<?php

namespace App\EntityListener;

use App\Entity\Pet\Contact;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class ContactListener
{
    private $security;

    public function __construct(
        Security $security
    ) {
        $this->security = $security;
    }

    /**
     * Undocumented function
     *
     * @param  Contact            $contact
     * @param  LifecycleEventArgs $lifecycleEventArgs
     * @return void
     */
    public function prePersist(Contact $contact, LifecycleEventArgs $lifecycleEventArgs): void
    {
        $user = $this->security->getUser();
        $pet = $lifecycleEventArgs->getObject()->getPet();

        $contact->setAdopter($user);
        $contact->setPet($pet);
    }
}
