<?php

namespace App\EntityListener;

use Ramsey\Uuid\Uuid;
use App\Entity\Account;
use App\Entity\User\User;
use App\Entity\User\Adopter;
use App\Entity\User\Shelter;
use Cocur\Slugify\SlugifyInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserListener
{
    private UserPasswordEncoderInterface $userPasswordEncoder;

    private SlugifyInterface $slugifyInterface;

    /**
     * Undocumented function
     *
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     * @param SlugifyInterface             $slugifyInterface
     */
    public function __construct(
        UserPasswordEncoderInterface $userPasswordEncoder,
        SlugifyInterface $slugifyInterface
    ) {
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->slugifyInterface = $slugifyInterface;
    }

    /**
     * Set some things before the entity is persisted
     *
     * @param  User $user
     * @return void
     */
    public function prePersist(User $user): void
    {
        $this->encodePassword($user);
        $this->setUuid($user);
        $this->setRoles($user);
        $this->setAccount($user);

        if ($user instanceof Shelter) {
            $this->setSlug($user);
        }
    }

    /**
     * Set some things before the entity is updated
     *
     * @param  User $user
     * @return void
     */
    public function preUpdate(User $user): void
    {
        if (!str_contains($user->getPassword(), '$argon2i')) {
            $this->encodePassword($user);
        }
    }

    /**
     * Undocumented function
     *
     * @param  User $user
     * @return void
     */
    private function encodePassword(User $user): void
    {
        if ($user->getPassword() === null) {
            return;
        }

        $user->setPassword(
            $this->userPasswordEncoder->encodePassword($user, $user->getPassword())
        );
    }

    /**
     * Undocumented function
     *
     * @param  User $user
     * @return void
     */
    private function setUuid(User $user): void
    {
        $user->setUuid(
            Uuid::uuid4()->toString()
        );
    }

    /**
     * Undocumented function
     *
     * @param  Shelter $shelter
     * @return void
     */
    private function setSlug(Shelter $shelter): void
    {
        $shelter->setSlug(
            $this->slugifyInterface->slugify($shelter->getName())
        );
    }

    /**
     * Undocumented function
     *
     * @param  User $user
     * @return void
     */
    private function setRoles(User $user)
    {
        if ($user instanceof Adopter) {
            $user->setRoles(
                [
                'ROLE_USER', 'ROLE_ADOPTER'
                ]
            );
        }

        if ($user instanceof Shelter) {
            $user->setRoles(
                [
                'ROLE_USER', 'ROLE_SHELTER'
                ]
            );
        }
    }

    /**
     * Undocumented function
     *
     * @param  User $user
     * @return void
     */
    private function setAccount(User $user)
    {
        $account = (new Account())
            ->setActivationToken(md5(uniqid()))
            ->setIsVerified(false);

        $user->setAccount($account);
    }
}
