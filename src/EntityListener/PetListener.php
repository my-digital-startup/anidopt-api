<?php

namespace App\EntityListener;

use Ramsey\Uuid\Uuid;
use App\Entity\Pet\Pet;
use Cocur\Slugify\SlugifyInterface;

class PetListener
{
    private SlugifyInterface $slugifyInterface;

    /**
     * Undocumented function
     *
     * @param SlugifyInterface $slugifyInterface
     */
    public function __construct(SlugifyInterface $slugifyInterface)
    {
        $this->slugifyInterface = $slugifyInterface;
    }

    /**
     * Set some things before the entity is persisted
     *
     * @param  Pet $pet
     * @return void
     */
    public function prePersist(Pet $pet): void
    {
        $uuid = Uuid::uuid4()->toString();
        $this->setUuid($pet, $uuid);
        $this->setSlug($pet, $uuid);
    }

    /**
     * Undocumented function
     *
     * @param  User $user
     * @return void
     */
    private function setUuid(Pet $pet, string $uuid): void
    {
        $pet->setUuid(
            $uuid
        );
    }

    /**
     * Undocumented function
     *
     * @param  Shelter $shelter
     * @return void
     */
    private function setSlug(Pet $pet, string $uuid): void
    {
        $slug = $pet->getName() . '-' . $uuid;
        $pet->setSlug(
            $this->slugifyInterface->slugify($slug)
        );
    }
}
