<?php

namespace App\Repository\Pet;

use App\Entity\Pet\Specie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Specie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Specie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Specie[]    findAll()
 * @method Specie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpecieRepository extends ServiceEntityRepository
{
    /**
     * Undocumented function
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Specie::class);
    }
}
