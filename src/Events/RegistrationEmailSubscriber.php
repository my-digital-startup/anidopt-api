<?php

namespace App\Events;

use App\Entity\User\Adopter;
use App\Entity\User\Shelter;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

final class RegistrationEmailSubscriber implements EventSubscriberInterface
{
    private MailerInterface $mailer;

    private VerifyEmailHelperInterface $verifyEmailHelper;

    /**
     * Undocumented function
     *
     * @param MailerInterface            $mailer
     * @param VerifyEmailHelperInterface $verifyEmailHelper
     */
    public function __construct(MailerInterface $mailer, VerifyEmailHelperInterface $verifyEmailHelper)
    {
        $this->mailer = $mailer;
        $this->verifyEmailHelper = $verifyEmailHelper;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['sendMail', EventPriorities::POST_WRITE],
        ];
    }

    /**
     * Send a verification email with a signed URL after registration
     *
     * @param  ViewEvent $event
     * @return void
     */
    public function sendMail(ViewEvent $event): void
    {
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        $path = $event->getRequest()->getPathInfo();

        if (
            ($user instanceof Adopter || $user instanceof Shelter)
            && $method === "POST"
            && ($path === "/api/v1/adopters" || $path === "/api/v1/shelters")
        ) {
            $signatureComponents = $this->verifyEmailHelper->generateSignature(
                'users_verify_account',
                $user->getAccount()->getActivationToken(),
                $user->getEmail(),
                ['activationToken' => $user->getAccount()->getActivationToken()]
            );

            $email = (new Email())
                ->from('no-reply.anidopt@juliendrieu.fr')
                ->to($user->getEmail())
                ->subject('Activation de votre compte')
                ->html(
                    '
                    <a href="' . $signatureComponents->getSignedUrl() . '">Cliquez ici pour valider votre compte</a>
                '
                );

            $this->mailer->send($email);
        }
    }
}
