<?php

// src/EventSubscriber/ExceptionSubscriber.php
namespace App\Events;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ExceptionSubscriber implements EventSubscriberInterface
{
    private TranslatorInterface $translator;

    /**
     * Undocumented function
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function getSubscribedEvents()
    {
        // return the subscribed events, their methods and priorities
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    /**
     * Intercepts exception to rewrite it
     *
     * @param  ExceptionEvent $event
     * @return void
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $throwable = $event->getThrowable();
        $statusCode = method_exists($throwable, 'getStatusCode') ? $throwable->getStatusCode() : 400;
        $response = $this->setGoodResponse($statusCode, $throwable);

        // sends the modified response object to the event
        // dd($response);
        $event->setResponse($response);
    }

    /**
     * Set good error message according to the tatus code
     *
     * @param  int $statusCode
     * @return void
     */
    private function setGoodResponse($statusCode, $throwable)
    {
        $detail = null;
        switch ($statusCode) {
            case 400:
                $detail = $this->translator->trans('Bad request');
                break;

            case 401:
                $detail = $this->translator->trans('Unauthorized');
                break;

            case 403:
                $detail = $this->translator->trans('Access Denied.');
                break;

            case 404:
                $detail = $this->translator->trans('Not found');
                break;

            case 500:
                $detail = $this->translator->trans('An error occurred');
                break;

            default:
                $detail = $this->translator->trans('An error occurred');
                break;
        }

        $message = [
            'status' => $statusCode,
            'detail' => $detail
        ];

        if (method_exists($throwable, 'getConstraintViolationList')) {
            if (count($throwable->getConstraintViolationList()) > 0) {
                $statusCode = 422;
                $message = [
                    'status' => 422,
                    'detail' => explode("\n", $throwable->getMessage())
                ];
            }
        }

        $response = new Response();
        $response->setStatusCode($statusCode);
        $response->setContent(json_encode($message));

        return $response;
    }
}
