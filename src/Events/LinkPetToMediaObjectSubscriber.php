<?php

namespace App\Events;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\MediaObject;
use App\Entity\User\Shelter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class LinkPetToMediaObjectSubscriber implements EventSubscriberInterface
{
    private TokenStorageInterface $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['checkMediaObjects', EventPriorities::PRE_WRITE],
        ];
    }

    /**
     * Check if media objects belong to user
     *
     * @param  ViewEvent $event
     * @return void
     */
    public function checkMediaObjects(ViewEvent $event)
    {
        $pet = $event->getControllerResult();
        $user = $user = $this->tokenStorage->getToken()->getUser();
        $method = $event->getRequest()->getMethod();
        $path = $event->getRequest()->getPathInfo();

        if (
            ($method === 'POST' || $method === 'PATCH')
            && (str_contains($path, '/api/v1/pets'))
            && ($user instanceof Shelter)
        ) {
            $petMainImage = $pet->getMainImage();
            $petImages = $pet->getImages()->toArray();
            $userImages = $user->getMediaObjects()->toArray();

            if ($petMainImage !== null && $petImages !== []) {
                if (!($this->checkPetImage($userImages, $petMainImage) && $this->checkPetImages($userImages, $petImages))) {
                    throw new HttpException(403, 'Certaines des images choisies ne vous appartiennent pas.');
                }
            }
        }
    }

    /**
     * Check if pet's image belong to user
     *
     * @param  array            $userImages
     * @param  MediaObject|null $mainImage
     * @return boolean
     */
    private function checkPetImage(array $userImages, ?MediaObject $mainImage): bool
    {
        foreach ($userImages as $image) {
            if ($mainImage === $image) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if pet's images belong to user
     *
     * @param  array $userImages
     * @param  array $petImages
     * @return boolean
     */
    private function checkPetImages(array $userImages, array $petImages): bool
    {
        $flags = [];
        foreach ($petImages as $petImage) {
            $flags[] = $this->checkPetImage($userImages, $petImage);
        }

        foreach ($flags as $flag) {
            if (!$flag) {
                return false;
            }
        }

        return true;
    }
}
