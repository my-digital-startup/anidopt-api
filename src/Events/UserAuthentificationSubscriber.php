<?php

namespace App\Events;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class UserAuthentificationSubscriber implements EventSubscriberInterface
{
    /**
     * Undocumented function
     *
     * @return void
     */
    public static function getSubscribedEvents()
    {
        return [
            Events::AUTHENTICATION_SUCCESS => 'onAuthenticationSuccess',
            Events::AUTHENTICATION_FAILURE => 'onAuthenticationFailure'
        ];
    }

    /**
     * Check if user is verified, if not, return an HTTP error
     *
     * @param  AuthenticationSuccessEvent $event
     * @return void
     */
    public function onAuthenticationSuccess(AuthenticationSuccessEvent $event)
    {
        $user = $event->getUser();

        if (!$user->getAccount()->getIsVerified()) {
            $event->setData(
                [
                'status' => 401,
                'detail' => 'Connexion impossible. L\'utilisateur n\'est pas validé.'
                ]
            );
            $event->getResponse()->setStatusCode(401);
        }
    }

    /**
     * Change response when authentication failed
     *
     * @param  AuthenticationFailureEvent $event
     * @return void
     */
    public function onAuthenticationFailure(AuthenticationFailureEvent $event)
    {
        $response = $event->getResponse();
        $response->setContent('{"status":401,"detail":"Informations d\'identification non valides."}');
        $event->setResponse($response);
    }
}
