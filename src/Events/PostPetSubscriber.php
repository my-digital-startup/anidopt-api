<?php

namespace App\Events;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User\Shelter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class PostPetSubscriber implements EventSubscriberInterface
{
    private TokenStorageInterface $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['postPet', EventPriorities::PRE_WRITE],
        ];
    }

    /**
     * Set a denied access if user is not a shelter
     *
     * @param  ViewEvent $event
     * @return void
     */
    public function postPet(ViewEvent $event): void
    {
        $user = $user = $this->tokenStorage->getToken()->getUser();
        $method = $event->getRequest()->getMethod();
        $path = $event->getRequest()->getPathInfo();

        if ($method === "POST" && $path === "/api/v1/pets") {
            if (!$user instanceof Shelter) {
                throw new HttpException(403);
            }
        }
    }
}
