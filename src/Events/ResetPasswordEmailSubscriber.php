<?php

namespace App\Events;

use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

final class ResetPasswordEmailSubscriber implements EventSubscriberInterface
{
    private MailerInterface $mailer;

    /**
     * Undocumented function
     *
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::RESPONSE => ['sendMail', EventPriorities::PRE_SERIALIZE],
        ];
    }

    /**
     * Send an email for reset user's password
     *
     * @param  ViewEvent $event
     * @return void
     */
    public function sendMail(ResponseEvent $event): void
    {
        $method = $event->getRequest()->getMethod();
        $path = $event->getRequest()->getPathInfo();
        $data = json_decode($event->getResponse()->getContent());

        if ($method === 'PUT' && $path === '/api/v1/users/password/reset' && property_exists($data, 'user')) {
            $resetPasswordToken = $data->user->resetPasswordToken;
            $emailAddress = $data->user->email;

            $url = $_ENV["RESET_PASSWORD_EMAIL_PATH"] . '?token=' . $resetPasswordToken . '?email=' . $emailAddress;

            $email = (new Email())
                ->from('hello@example.com')
                ->to($emailAddress)
                ->subject('Réinitialisation de votre mot de passe')
                ->html(
                    '
                    <a href="' . $url . '">Cliquez ici pour renseigner un nouveau mot de passe</a>
                '
                );

            $this->mailer->send($email);
        }
    }
}
