<?php

namespace App\Events;

use App\Entity\User\Adopter;
use App\Entity\User\Shelter;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class ChangeEmailSubscriber implements EventSubscriberInterface
{
    private MailerInterface $mailer;

    /**
     * Undocumented function
     *
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['sendMail', EventPriorities::POST_WRITE],
        ];
    }

    /**
     * Send an email to the new email choose by the user
     *
     * @param  ViewEvent $event
     * @return void
     */
    public function sendMail(ViewEvent $event): void
    {
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        $path = $event->getRequest()->getPathInfo();

        if (
            ($user instanceof Adopter || $user instanceof Shelter)
            && $method === "PATCH"
            && ($path === "/api/v1/users/" . $user->getUuid() . '/email')
        ) {
            $email = (new Email())
                ->from('no-reply@anidopt.bzh')
                ->to($user->getEmail())
                ->subject('Changement d\'adresse email')
                ->html(
                    '
                    <p>Vous avez choisi cette adresse email pour votre compte Anidopt</p>
                '
                );

            $this->mailer->send($email);
        }
    }
}
