<?php

namespace App\Events;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTInvalidEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class JwtSubscriber implements EventSubscriberInterface
{
    /**
     * Undocumented function
     *
     * @return void
     */
    public static function getSubscribedEvents()
    {
        return [
            Events::JWT_EXPIRED => 'onJwtExpired',
            Events::JWT_INVALID => 'onJwtInvalid',
            EVENTS::JWT_NOT_FOUND => 'onJwtNotFound'
        ];
    }

    /**
     * Send a particular response if the JWT is expired
     *
     * @param  JWTExpiredEvent $event
     * @return void
     */
    public function onJwtExpired(JWTExpiredEvent $event)
    {
        $response = $event->getResponse();
        $response->setContent('{"status":401,"detail":"L\'élément nécéssaire à la connexion est expiré. Veuillez réessayer."}');
        $event->setResponse($response);
    }

    /**
     * Send a particular response if the JWT is invalid
     *
     * @param  JWTInvalidEvent $event
     * @return void
     */
    public function onJwtInvalid(JWTInvalidEvent $event)
    {
        $response = $event->getResponse();
        $response->setContent('{"status":401,"detail":"L\'élément nécéssaire à la connexion est invalide. Veuillez réessayer."}');
        $event->setResponse($response);
    }

    /**
     * Send a particular response if the JWT is Ressource non trouvée.
     *
     * @param  JWTNotFoundEvent $event
     * @return void
     */
    public function onJwtNotFound(JWTNotFoundEvent $event)
    {
        $response = $event->getResponse();
        $response->setContent('{"status":401,"detail":"L\'élément nécéssaire à la connexion n\'a pas été trouvé. Veuillez réessayer."}');
        $event->setResponse($response);
    }
}
