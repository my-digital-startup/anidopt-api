<?php

namespace App\Events;

use App\Entity\User\Adopter;
use App\Entity\User\Shelter;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User\User;
use App\Service\EmailManager;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class ContactSubscriber implements EventSubscriberInterface
{
    private MailerInterface $mailer;
    private EmailManager $emailManager;
    private NormalizerInterface $normalizerInterface;
    private Security $security;

    /**
     * Undocumented function
     *
     * @param MailerInterface     $mailer
     * @param EmailManager        $emailManager
     * @param NormalizerInterface $normalizerInterface
     * @param Security            $security
     */
    public function __construct(
        MailerInterface $mailer,
        EmailManager $emailManager,
        NormalizerInterface $normalizerInterface,
        Security $security
    ) {
        $this->mailer = $mailer;
        $this->emailManager = $emailManager;
        $this->normalizerInterface = $normalizerInterface;
        $this->security = $security;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['sendMail', EventPriorities::PRE_WRITE],
        ];
    }

    public function sendMail(ViewEvent $event)
    {
        $method = $event->getRequest()->getMethod();
        $path = $event->getRequest()->getPathInfo();
        $user = $this->security->getUser();

        if ($user instanceof Adopter && $method === 'POST'  && $path == '/api/v1/contacts') {
            $pet = $event->getControllerResult()->getPet();
            $shelter = $event->getControllerResult()->getPet()->getShelter();

            $normalizedPet = $this->normalizerInterface->normalize($pet, null, []);
            $normalizedShelter = $this->normalizerInterface->normalize($shelter, null, []);
            $normalizedUser = $this->normalizerInterface->normalize($this->security->getUser(), null, []);

            $this->emailManager->sendEmail(
                'email/contact/index.html.twig',
                $shelter,
                [
                    'pet' => $normalizedPet,
                    'shelter' => $normalizedShelter,
                    'user' => $normalizedUser,
                    // 'content' => $content
                ],
                'Anidopt <no-reply@anidopt.bzh>',
                'New contact !',
                null
            );
        }
        return;
    }
}
