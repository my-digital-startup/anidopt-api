<?php

namespace App\DataFixtures;

use Faker\Factory;
use Faker\Generator;
use App\Entity\User\User;
use Doctrine\Persistence\ObjectManager;
use App\DataFixtures\User\AdopterFixtures;
use App\DataFixtures\User\ShelterFixtures;
use App\Repository\User\AdopterRepository;
use App\Repository\User\ShelterRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class AccountFixtures extends Fixture implements DependentFixtureInterface
{
    private Generator $faker;

    private AdopterRepository $adopterRepository;

    private ShelterRepository $shelterRepository;

    /**
     * Undocumented function
     *
     * @param Generator $faker
     */
    public function __construct(AdopterRepository $adopterRepository, ShelterRepository $shelterRepository)
    {
        $this->faker = Factory::create('fr_FR');
        $this->adopterRepository = $adopterRepository;
        $this->shelterRepository = $shelterRepository;
    }

    /**
     * Get fixtures that need to be launched before this fixture
     *
     * @return void
     */
    public function getDependencies()
    {
        return [
            AdopterFixtures::class,
            ShelterFixtures::class,
        ];
    }

    /**
     * Load account fixtures
     *
     * @param  ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $adopters = $this->adopterRepository->findAll();
        $shelters = $this->shelterRepository->findAll();

        /* Set adopters's account */
        foreach ($adopters as $index => $user) {
            $manager->persist($this->setAccountInformations($user, $index));
        }

        /* Set shelters's account */
        foreach ($shelters as $index => $user) {
            $manager->persist($this->setAccountInformations($user, $index));
        }

        $manager->flush();
    }

    /**
     * Set account informations
     *
     * @param  User $user
     * @return User
     */
    private function setAccountInformations(User $user, $index): User
    {
        if ($index === 0) {
            $user->getAccount()
                ->setIsVerified(true)
                ->setActivationToken(null)
                ->setAddress($this->faker->streetAddress)
                ->setCity($this->faker->city)
                ->setZipCode($this->faker->postcode);
        }

        $user->getAccount()
            ->setAddress($this->faker->streetAddress)
            ->setCity($this->faker->city)
            ->setZipCode($this->faker->postcode);

        return $user;
    }
}
