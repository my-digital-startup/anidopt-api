<?php

namespace App\DataFixtures;

use Faker\Factory;
use Faker\Generator;
use Ramsey\Uuid\Uuid;
use DateTimeImmutable;
use App\Entity\Pet\Pet;
use App\Entity\Pet\Tag;
use App\Entity\Pet\Breed;
use App\Entity\Pet\Specie;
use App\Entity\User\Shelter;
use App\Repository\MediaObjectRepository;
use Doctrine\Persistence\ObjectManager;
use App\Repository\User\ShelterRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class PetFixtures extends Fixture implements DependentFixtureInterface
{
    private Generator $faker;

    private ShelterRepository $shelterRepository;

    private MediaObjectRepository $mediaObjectRepository;

    private array $species;
    private array $dogBreeds;
    private array $catBreeds;
    private array $otherBreeds;
    private array $tags;
    private array $pets;

    /**
     * Undocumented function
     *
     * @param Generator $faker
     */
    public function __construct(ShelterRepository $shelterRepository, MediaObjectRepository $mediaObjectRepository)
    {
        $this->faker = Factory::create('fr_FR');
        $this->shelterRepository = $shelterRepository;
        $this->mediaObjectRepository = $mediaObjectRepository;

        $this->species = [];
        $this->dogBreeds = [];
        $this->catBreeds = [];
        $this->otherBreeds = [];
        $this->tags = [];
        $this->pets = [];
    }

    /**
     * Get fixtures that need to be launched before this fixture
     *
     * @return void
     */
    public function getDependencies()
    {
        return [
            MediaObjectFixtures::class,
        ];
    }

    /**
     * Load account fixtures
     *
     * @param  ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        /* Set species */
        $speciesName = ['Chat', 'Chien', 'NAC'];
        foreach ($speciesName as $specieName) {
            $manager->persist($this->setSpecie($specieName));
        }

        /* Set breeds */
        for ($i = 0; $i < 10; $i++) {
            $manager->persist($this->setDogBreed($i));
            $manager->persist($this->setCatBreed($i));
            $manager->persist($this->setOtherBreed($i));
        }

        /* Set tags */
        for ($i = 0; $i < 10; $i++) {
            $manager->persist($this->setTag($i));
        }

        /* Set pets */
        $shelters = $this->shelterRepository->findAll();
        foreach ($shelters as $shelter) {
            for ($i = 0; $i < mt_rand(3, 6); $i++) {
                $manager->persist($this->setPet($shelter, 'dog'));
                $manager->persist($this->setPet($shelter, 'cat'));
                $manager->persist($this->setPet($shelter, 'other'));
            }
        }

        $manager->flush();
    }

    /**
     * Undocumented function
     *
     * @param  string $name
     * @return void
     */
    private function setSpecie(string $name): Specie
    {
        $specie = (new Specie())
            ->setName($name);

        $this->species[] = $specie;
        return $specie;
    }

    /**
     * Set dog breeds
     *
     * @return Breed
     */
    private function setDogBreed(int $index): Breed
    {
        $breed = (new Breed())
            ->setName('Dog breed ' . $index . $this->faker->word);

        $this->dogBreeds[] = $breed;
        return $breed;
    }

    /**
     * Set cats breeds
     *
     * @return Breed
     */
    private function setCatBreed(int $index): Breed
    {
        $breed = (new Breed())
            ->setName('Cat breed ' . $index  . $this->faker->word);

        $this->catBreeds[] = $breed;
        return $breed;
    }

    /**
     * Set other breeds
     *
     * @return Breed
     */
    private function setOtherBreed(int $index): Breed
    {
        $breed = (new Breed())
            ->setName('Other breed ' . $index  . $this->faker->word);

        $this->otherBreeds[] = $breed;
        return $breed;
    }

    /**
     * Set tag
     *
     * @return Tag
     */
    private function setTag(int $index): Tag
    {
        $tag = (new Tag())
            ->setName('Tag ' . $index . $this->faker->word);

        $this->tags[] = $tag;
        return $tag;
    }

    /**
     * Set pet
     *
     * @param  Shelter     $shelter
     * @param  string|null $type
     * @return Pet
     */
    private function setPet(Shelter $shelter, ?string $type = 'dog'): Pet
    {
        $pet = (new Pet())
            ->setName($this->faker->name)
            ->setDescription($this->faker->text)
            ->setIsMale(mt_rand(0, 1) === 1 ? true : false)
            ->setBirthday(new DateTimeImmutable())
            ->setIsSterilised(mt_rand(0, 1) === 1 ? true : false)
            ->setIdentificationNumber(Uuid::uuid4()->toString())
            ->setIsSos(mt_rand(0, 1) === 1 ? true : false)
            ->setShelter($shelter)
            ->setSpecie($this->species[mt_rand(0, count($this->species) - 1)])
            ->setMainImage($this->mediaObjectRepository->findOneBy([]));

        $breeds = $this->chooseBreed($type);
        $pet->addBreed($breeds[(mt_rand(0, count($breeds) - 1))]);

        foreach ($this->tags as $tag) {
            if (mt_rand(0, 2) === 1) {
                $pet->addTag($tag);
            }
        }

        for ($i = 0; $i < 2; $i++) {
            $pet->addImage($this->mediaObjectRepository->findOneBy([]));
        }

        $this->pets[] = $pet;

        return $pet;
    }

    /**
     * Choose good breeds
     *
     * @param  string $type
     * @return array
     */
    private function chooseBreed(string $type): array
    {
        $breeds = null;
        switch ($type) {
            case 'dog':
                $breeds = $this->dogBreeds;
                break;

            case 'cat':
                $breeds = $this->catBreeds;
                break;

            case 'other':
                $breeds = $this->otherBreeds;
                break;

            default:
                $breeds = $this->dogBreeds;
                break;
        }

        return $breeds;
    }
}
