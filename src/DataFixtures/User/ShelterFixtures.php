<?php

namespace App\DataFixtures\User;

use App\Entity\User\Shelter;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class ShelterFixtures extends Fixture
{
    private Generator $faker;

    /**
     * Undocumented function
     *
     * @param Generator $faker
     */
    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }

    /**
     * Load shelter fixtures
     *
     * @param  ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        /* Setting up shelter anidopt */
        $shelter = (new Shelter())
            ->setName($this->faker->name())
            ->setEmail('shelter@anidopt.bzh')
            ->setPassword('password');

        $manager->persist($shelter);

        /* Setting up others shelters */
        for ($i = 0; $i < 5; $i++) {
            $manager->persist($this->createdShelter());
        }

        $manager->flush();
    }

    /**
     * Create a shelter
     *
     * @return Shelter
     */
    private function createdShelter(): Shelter
    {
        $shelter = (new Shelter())
            ->setName($this->faker->name)
            ->setEmail($this->faker->email)
            ->setPassword('password');

        return $shelter;
    }
}
