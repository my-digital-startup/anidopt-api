<?php

namespace App\DataFixtures\User;

use App\DataFixtures\PetFixtures;
use Faker\Factory;
use Faker\Generator;
use App\Entity\User\Adopter;
use App\Repository\Pet\PetRepository;
use App\Repository\User\ShelterRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class AdopterFixtures extends Fixture implements DependentFixtureInterface
{
    private Generator $faker;

    private array $adopters;

    private PetRepository $petRepository;

    private ShelterRepository $shelterRepository;

    /**
     * Undocumented function
     *
     * @param Generator $faker
     */
    public function __construct(PetRepository $petRepository, ShelterRepository $shelterRepository)
    {
        $this->faker = Factory::create('fr_FR');
        $this->adopters = [];
        $this->petRepository = $petRepository;
        $this->shelterRepository = $shelterRepository;
    }

    /**
     * Get fixtures that need to be launched before this fixture
     *
     * @return void
     */
    public function getDependencies()
    {
        return [
            PetFixtures::class,
        ];
    }

    /**
     * Load adopter fixtures
     *
     * @param  ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $pets = $this->petRepository->findAll();

        /* Setting up anidopt adopter */
        $adopter = (new Adopter())
            ->setFirstName(mt_rand(0, 1) === 1 ? $this->faker->firstName : '')
            ->setLastName(mt_rand(0, 1) === 1 ? $this->faker->lastName : '')
            ->setPseudo(mt_rand(0, 1) === 1 ? $this->faker->name() : '')
            ->setEmail('adopter@anidopt.bzh')
            ->setPassword('password');

        $this->adopters[] = $adopter;
        $manager->persist($adopter);

        /* Setting up others adopters */
        for ($i = 0; $i < 15; $i++) {
            $manager->persist($this->createAdopter($pets));
        }

        /* Setting up favorites pets */
        foreach ($this->adopters as $adopter) {
            foreach ($pets as $pet) {
                $pet->getIsMale() ? $adopter->addFavoritePet($pet) : '';
            }
        }

        /* Setting up favorite shelters */
        $shelters = $this->shelterRepository->findAll();

        foreach ($shelters as $shelter) {
            $shelter->addAdoptersWhoHaveLiked($this->adopters[mt_rand(0, count($this->adopters) - 1)]);
        }

        $manager->flush();
    }

    /**
     * Create an adopter
     *
     * @return Adopter
     */
    private function createAdopter(): Adopter
    {
        $adopter = (new Adopter())
            ->setFirstName(mt_rand(0, 1) === 1 ? $this->faker->firstName : '')
            ->setLastName(mt_rand(0, 1) === 1 ? $this->faker->lastName : '')
            ->setPseudo(mt_rand(0, 1) === 1 ? $this->faker->name() : '')
            ->setEmail($this->faker->email)
            ->setPassword('password');

        $this->adopters[] = $adopter;
        return $adopter;
    }
}
