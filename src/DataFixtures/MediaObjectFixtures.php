<?php

namespace App\DataFixtures;

use App\DataFixtures\User\ShelterFixtures;
use App\Entity\MediaObject;
use App\Entity\User\Shelter;
use Doctrine\Persistence\ObjectManager;
use App\Repository\User\ShelterRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class MediaObjectFixtures extends Fixture implements DependentFixtureInterface
{
    private ShelterRepository $shelterRepository;

    /**
     * Undocumented function
     *
     * @param ShelterRepository $shelterRepository
     */
    public function __construct(ShelterRepository $shelterRepository)
    {
        $this->shelterRepository = $shelterRepository;
    }

    /**
     * Get fixtures that need to be launched before this fixture
     *
     * @return void
     */
    public function getDependencies()
    {
        return [
            ShelterFixtures::class,
        ];
    }

    /**
     * Load account fixtures
     *
     * @param  ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $shelter = $this->shelterRepository->findOneBy([]);

        copy(__DIR__ . '/Ressources/' . 'placeholder.jpeg', __DIR__ . '/Ressources/' . 'placeholder-copy.jpeg');
        $src = (__DIR__ . '/Ressources/'  . 'placeholder-copy.jpeg');

        $file = new UploadedFile($src, 'placeholder.jpeg', 'application/octet-stream', null, true);

        $media = new MediaObject();
        $media->file = $file;
        $media->setAuthor($shelter);

        $manager->persist($media);

        $manager->flush();
    }
}
