<?php

namespace App\Tests\Unit;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class EntityTest extends KernelTestCase
{
    /**
     * Determines whether a test has passed by counting the number of errors generated against the expected number of errors
     *
     * @param Object $entity
     * @param integer $number
     * @return void
     */
    public function assertHasErrors(object $entity, int $number = 0): void
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($entity);
        $messages = [];

        /** @var ConstraintViolation $error */
        foreach ($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }

        $this->assertCount($number, $errors, implode(', ', $messages));
    }
}
