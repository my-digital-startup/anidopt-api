<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;

class AllIsGoodUnitTest extends TestCase
{
    /**
     * Simple test to check if unit tests are OK
     *
     * @return void
     */
    public function testAllIsGood()
    {
        $this->assertEquals(4, 2 + 2);
    }
}
