<?php

namespace App\Tests\Unit\Pet;

use DateTimeImmutable;
use Faker\Factory;
use App\Entity\Pet\Pet;
use App\Entity\Pet\Tag;
use App\Entity\Pet\Breed;
use App\Entity\Pet\Specie;
use App\Entity\User\Shelter;
use App\Tests\Unit\EntityTest;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Uid\Uuid;

class PetTest extends EntityTest
{
    /**
     * Get a correct entity of Pet
     *
     * @return Pet
     */
    public function getEntity(): Pet
    {
        $faker = Factory::create();

        $shelter = (new Shelter())
            ->setUuid(Uuid::v4())
            ->setEmail($faker->email)
            ->setPassword('password')
            ->setCreatedAt(new \DateTime())
            ->setName($faker->name)
            ->setSlug($faker->slug);

        $specie = (new Specie())
            ->setName($faker->name);

        $breed = (new Breed())
            ->setName($faker->name);

        $tag = (new Tag())
            ->setName($faker->name);

        return (new Pet())
            ->setUuid(Uuid::v4())
            ->setName($faker->name)
            ->setDescription($faker->text)
            ->setIsMale(true)
            ->setBirthday(new DateTimeImmutable())
            ->setSlug($faker->slug)
            ->setIsSterilised(true)
            ->setIdentificationNumber(Uuid::v4())
            ->setIsSos(true)
            ->setShelter($shelter)
            ->setSpecie($specie)
            ->addBreed($breed)
            ->addTag($tag);
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check getters and setters
     *
     * @return void
     */
    public function testGettersAndSettersAreWorking()
    {
        $uuid = Uuid::v4();
        $createdAt = new DateTimeImmutable();

        $shelter = (new Shelter())
            ->setUuid(Uuid::v4())
            ->setEmail('email@anidopt.bzh')
            ->setPassword('password')
            ->setCreatedAt(new \DateTime())
            ->setName('Shelter')
            ->setSlug('shelter');

        $specie = (new Specie())
            ->setName('specie');

        $pet = (new Pet())
            ->setUuid($uuid)
            ->setName('Pet')
            ->setDescription('description')
            ->setIsMale(true)
            ->setBirthday($createdAt)
            ->setSlug('pet')
            ->setIsSterilised(true)
            ->setIdentificationNumber($uuid)
            ->setIsSos(true)
            ->setShelter($shelter)
            ->setSpecie($specie)
            ->setCreatedAt($createdAt);

        $this->assertEquals($uuid, $pet->getUuid());
        $this->assertEquals('Pet', $pet->getName());
        $this->assertEquals('description', $pet->getDescription());
        $this->assertEquals(true, $pet->getIsMale());
        $this->assertEquals($createdAt, $pet->getBirthday());
        $this->assertEquals('pet', $pet->getSlug());
        $this->assertEquals(true, $pet->getIsSterilised());
        $this->assertEquals($uuid, $pet->getIdentificationNumber());
        $this->assertEquals(true, $pet->getIsSos());
        $this->assertEquals($shelter, $pet->getShelter());
        $this->assertEquals($specie, $pet->getSpecie());
        $this->assertEquals($createdAt, $pet->getCreatedAt());
    }

    /**
     * Check if the attribute name respects its constraints
     *
     * @return void
     */
    public function testNameIsInvalid()
    {
        $faker = Factory::create();
        $this->assertHasErrors($this->getEntity()->setName(''), 1);
        $this->assertHasErrors($this->getEntity()->setName(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute slug respects its constraints
     *
     * @return void
     */
    public function testSlugIsInvalid()
    {
        $faker = Factory::create();
        $this->assertHasErrors($this->getEntity()->setSlug(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute slug respects its constraints
     *
     * @return void
     */
    public function testIdentifierIsInvalid()
    {
        $faker = Factory::create();
        $this->assertHasErrors($this->getEntity()->setIdentificationNumber(implode(" ", $faker->words(100))), 1);
    }
}
