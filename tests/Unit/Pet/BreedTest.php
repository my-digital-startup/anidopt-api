<?php

namespace App\Tests\Unit\Pet;

use DateTimeImmutable;
use Faker\Factory;
use App\Entity\Pet\Breed;
use App\Tests\Unit\EntityTest;

class BreedTest extends EntityTest
{
    /**
     * Get a correct entity of Breed
     *
     * @return Breed
     */
    public function getEntity(): Breed
    {
        $faker = Factory::create();

        return (new Breed())
            ->setName('Breed')
            ->setcreatedAt(new DateTimeImmutable());
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check getters and setters
     *
     * @return void
     */
    public function testGettersAndSettersAreWorking()
    {
        $createdAt = new DateTimeImmutable();

        $breed = (new Breed())
            ->setName('Breed')
            ->setcreatedAt($createdAt);

        $this->assertEquals('Breed', $breed->getName());
        $this->assertEquals($createdAt, $breed->getCreatedAt());
    }

    /**
     * Check if the attribute name respects its constraints
     *
     * @return void
     */
    public function testNameIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setName(''), 1);
        $this->assertHasErrors($this->getEntity()->setName(implode(" ", $faker->words(100))), 1);
    }
}
