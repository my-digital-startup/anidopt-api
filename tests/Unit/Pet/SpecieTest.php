<?php

namespace App\Tests\Unit\Pet;

use DateTimeImmutable;
use Faker\Factory;
use App\Entity\Pet\Specie;
use App\Tests\Unit\EntityTest;

class SpecieTest extends EntityTest
{
    /**
     * Get a correct entity of Specie
     *
     * @return Specie
     */
    public function getEntity(): Specie
    {
        $faker = Factory::create();

        return (new Specie())
            ->setName('Specie')
            ->setcreatedAt(new DateTimeImmutable());
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check getters and setters
     *
     * @return void
     */
    public function testGettersAndSettersAreWorking()
    {
        $createdAt = new DateTimeImmutable();

        $specie = (new Specie())
            ->setName('Specie')
            ->setcreatedAt($createdAt);

        $this->assertEquals('Specie', $specie->getName());
        $this->assertEquals($createdAt, $specie->getCreatedAt());
    }

    /**
     * Check if the attribute name respects its constraints
     *
     * @return void
     */
    public function testNameIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setName(''), 1);
        $this->assertHasErrors($this->getEntity()->setName(implode(" ", $faker->words(100))), 1);
    }
}
