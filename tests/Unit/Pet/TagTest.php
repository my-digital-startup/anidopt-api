<?php

namespace App\Tests\Unit\Pet;

use DateTimeImmutable;
use Faker\Factory;
use App\Entity\Pet\Tag;
use App\Tests\Unit\EntityTest;

class TagTest extends EntityTest
{
    /**
     * Get a correct entity of Tag
     *
     * @return Tag
     */
    public function getEntity(): Tag
    {
        $faker = Factory::create();

        return (new Tag())
            ->setName('Tag')
            ->setcreatedAt(new DateTimeImmutable());
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check getters and setters
     *
     * @return void
     */
    public function testGettersAndSettersAreWorking()
    {
        $createdAt = new DateTimeImmutable();

        $tag = (new Tag())
            ->setName('Tag')
            ->setcreatedAt($createdAt);

        $this->assertEquals('Tag', $tag->getName());
        $this->assertEquals($createdAt, $tag->getCreatedAt());
    }

    /**
     * Check if the attribute name respects its constraints
     *
     * @return void
     */
    public function testNameIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setName(''), 1);
        $this->assertHasErrors($this->getEntity()->setName(implode(" ", $faker->words(100))), 1);
    }
}
