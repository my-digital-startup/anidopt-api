<?php

namespace App\Tests\Unit;

use App\Entity\Account;
use Faker\Factory;
use App\Entity\User\Adopter;
use App\Tests\Unit\EntityTest;
use Symfony\Component\Uid\Uuid;

class AccountTest extends EntityTest
{
    /**
     * Get a correct entity of Account
     *
     * @return Account
     */
    public function getEntity(): Account
    {
        $faker = Factory::create();

        $adopter = (new Adopter())
            ->setUuid(Uuid::v4())
            ->setEmail($faker->email)
            ->setPassword('password')
            ->setCreatedAt(new \DateTime())
            ->setFirstName($faker->firstName)
            ->setLastName($faker->lastName)
            ->setPseudo($faker->name);

        return (new Account())
            ->setIsVerified(false)
            ->setUser($adopter);
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check getters and setters
     *
     * @return void
     */
    public function testGettersAndSettersAreWorking()
    {
        $uuid = Uuid::v4();
        $createdAt = new \DateTime();

        $adopter = (new Adopter())
            ->setUuid($uuid)
            ->setEmail('email@anidopt.bzh')
            ->setRoles(['ROLE_USER', 'ROLE_ADOPTER'])
            ->setPassword('password')
            ->setCreatedAt($createdAt)
            ->setFirstName('firstName')
            ->setLastName('lastName')
            ->setPseudo('pseudo');

        $account = (new Account())
            ->setActivationToken(null)
            ->setIsVerified(false)
            ->setUser($adopter);

        $this->assertEquals(null, $account->getActivationToken());
        $this->assertEquals(false, $account->getIsVerified());
        $this->assertEquals($adopter, $account->getUser());
        $this->assertEquals(null, $account->getResetPasswordToken());
    }

    /**
     * Check if the attribute activation token respects its constraints
     *
     * @return void
     */
    public function testActivationTokenIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setActivationToken(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute activation token respects its constraints
     *
     * @return void
     */
    public function testResetPasswordTokenIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setResetPasswordToken(implode(" ", $faker->words(100))), 1);
    }
}
