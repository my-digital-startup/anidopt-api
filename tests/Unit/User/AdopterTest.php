<?php

namespace App\Tests\Unit\User;

use Faker\Factory;
use App\Entity\User\Adopter;
use App\Tests\Unit\EntityTest;
use Symfony\Component\Uid\Uuid;

class AdopterTest extends EntityTest
{
    /**
     * Get a correct entity of Adopter
     *
     * @return Adopter
     */
    public function getEntity(): Adopter
    {
        $faker = Factory::create();

        return (new Adopter())
            ->setUuid(Uuid::v4())
            ->setEmail($faker->email)
            ->setPassword('password')
            ->setCreatedAt(new \DateTime())
            ->setFirstName($faker->firstName)
            ->setLastName($faker->lastName)
            ->setPseudo($faker->name);
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check getters and setters
     *
     * @return void
     */
    public function testGettersAndSettersAreWorking()
    {
        $uuid = Uuid::v4();
        $createdAt = new \DateTime();

        $adopter = (new Adopter())
            ->setUuid($uuid)
            ->setEmail('email@anidopt.bzh')
            ->setRoles(['ROLE_USER', 'ROLE_ADOPTER'])
            ->setPassword('password')
            ->setCreatedAt($createdAt)
            ->setFirstName('firstName')
            ->setLastName('lastName')
            ->setPseudo('pseudo');

        $this->assertEquals($uuid, $adopter->getUuid());
        $this->assertEquals('email@anidopt.bzh', $adopter->getEmail());
        $this->assertEquals(['ROLE_USER', 'ROLE_ADOPTER'], $adopter->getRoles());
        $this->assertEquals('password', $adopter->getPassword());
        $this->assertEquals($createdAt, $adopter->getCreatedAt());
        $this->assertEquals('firstName', $adopter->getFirstName());
        $this->assertEquals('lastName', $adopter->getLastName());
        $this->assertEquals('pseudo', $adopter->getPseudo());
    }

    /**
     * Check if the attribute email respects its constraints
     *
     * @return void
     */
    public function testEmailIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setEmail(''), 2);
        $this->assertHasErrors($this->getEntity()->setEmail('E'), 2);
        $this->assertHasErrors($this->getEntity()->setEmail(implode(" ", $faker->words(100))), 2);
    }

    /**
     * Check if the attribute password respects its constraints
     *
     * @return void
     */
    public function testPasswordIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setPassword(''), 2);
        $this->assertHasErrors($this->getEntity()->setPassword(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute firstName respects its constraints
     *
     * @return void
     */
    public function testFirstNameIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setFirstName(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute lastName respects its constraints
     *
     * @return void
     */
    public function testLastNameIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setLastName(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute pseudo respects its constraints
     *
     * @return void
     */
    public function testPseudoIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setPseudo(implode(" ", $faker->words(100))), 1);
    }
}
