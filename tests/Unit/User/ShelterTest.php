<?php

namespace App\Tests\Unit\User;

use Faker\Factory;
use App\Entity\User\Shelter;
use App\Tests\Unit\EntityTest;
use Symfony\Component\Uid\Uuid;

class ShelterTest extends EntityTest
{
    /**
     * Get a correct entity of Shelter
     *
     * @return Shelter
     */
    public function getEntity(): Shelter
    {
        $faker = Factory::create();

        return (new Shelter())
            ->setUuid(Uuid::v4())
            ->setEmail($faker->email)
            ->setPassword('password')
            ->setCreatedAt(new \DateTime())
            ->setName($faker->name)
            ->setSlug($faker->slug);
    }

    /**
     * Check if the entity of getEntity() is correct
     *
     * @return void
     */
    public function testEntityIsValid(): void
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    /**
     * Check getters and setters
     *
     * @return void
     */
    public function testGettersAndSettersAreWorking()
    {
        $uuid = Uuid::v4();
        $createdAt = new \DateTime();

        $shelter = (new Shelter())
            ->setUuid($uuid)
            ->setEmail('email@anidopt.bzh')
            ->setRoles(['ROLE_USER', 'ROLE_SHELTER'])
            ->setPassword('password')
            ->setCreatedAt($createdAt)
            ->setName('name')
            ->setSlug('name');

        $this->assertEquals($uuid, $shelter->getUuid());
        $this->assertEquals('email@anidopt.bzh', $shelter->getEmail());
        $this->assertEquals(['ROLE_USER', 'ROLE_SHELTER'], $shelter->getRoles());
        $this->assertEquals('password', $shelter->getPassword());
        $this->assertEquals($createdAt, $shelter->getCreatedAt());
        $this->assertEquals('name', $shelter->getName());
        $this->assertEquals('name', $shelter->getSlug());
    }

    /**
     * Check if the attribute email respects its constraints
     *
     * @return void
     */
    public function testEmailIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setEmail(''), 2);
        $this->assertHasErrors($this->getEntity()->setEmail('E'), 2);
        $this->assertHasErrors($this->getEntity()->setEmail(implode(" ", $faker->words(100))), 2);
    }

    /**
     * Check if the attribute password respects its constraints
     *
     * @return void
     */
    public function testPasswordIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setPassword(''), 2);
        $this->assertHasErrors($this->getEntity()->setPassword(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute name respects its constraints
     *
     * @return void
     */
    public function testNameIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setName(''), 2);
        $this->assertHasErrors($this->getEntity()->setName('E'), 1);
        $this->assertHasErrors($this->getEntity()->setName(implode(" ", $faker->words(100))), 1);
    }

    /**
     * Check if the attribute slug respects its constraints
     *
     * @return void
     */
    public function testSlugIsInvalid()
    {
        $faker = Factory::create();

        $this->assertHasErrors($this->getEntity()->setSlug('E'), 1);
        $this->assertHasErrors($this->getEntity()->setSlug(implode(" ", $faker->words(100))), 1);
    }
}
