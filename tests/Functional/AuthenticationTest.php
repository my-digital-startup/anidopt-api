<?php

namespace App\Tests\Functional;

use App\Entity\User\User;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Repository\User\UserRepository;
use Doctrine\ORM\EntityManager;

class AuthenticationTest extends ApiTestCase
{
    private EntityManager $entityManager;

    private UserRepository $userRepository;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->userRepository = $this->entityManager
            ->getRepository(User::class);
    }

    /**
     * Test authentication if user is not valided
     *
     * @return void
     */
    public function testAuthenticationIfUserIsNotValidated()
    {
        $user = $this->userRepository->findOneBy(['id' => 2]);

        $this->createClient()->request('POST', '/api/v1/login_check', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'username' => $user->getEmail(),
                'password' => 'password'
            ],
        ]);

        $this->assertResponseStatusCodeSame(401);
        $this->assertJsonContains(['status' => 401]);
        $this->assertJsonContains(['detail' => 'Connexion impossible. L\'utilisateur n\'est pas validé.']);
    }

    /**
     * Test authentication with an empty json
     *
     * @return void
     */
    public function testAuthenticationWitAnEmptyJson()
    {
        $this->createClient()->request('POST', '/api/v1/login_check', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [],
        ]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertJsonContains(['status' => 400]);
        $this->assertJsonContains(['detail' => 'Mauvaise requête.']);
    }

    /**
     * Test authentication withut username
     *
     * @return void
     */
    public function testAuthenticationWithoutUsername()
    {
        $this->createClient()->request('POST', '/api/v1/login_check', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'password' => 'password'
            ],
        ]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertJsonContains(['status' => 400]);
        $this->assertJsonContains(['detail' => 'Mauvaise requête.']);
    }

    /**
     * Test authentication without password
     *
     * @return void
     */
    public function testAuthenticationWithoutPassword()
    {
        $this->createClient()->request('POST', '/api/v1/login_check', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'username' => 'adopter@anidopt.bzh'
            ],
        ]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertJsonContains(['status' => 400]);
        $this->assertJsonContains(['detail' => 'Mauvaise requête.']);
    }

    /**
     * Test authentication with wrong credentials
     *
     * @return void
     */
    public function testAuthenticationWithWrongCredentials()
    {
        $this->createClient()->request('POST', '/api/v1/login_check', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'username' => 'adopter@anidopt.bzh',
                'password' => 'pssword'
            ],
        ]);

        $this->assertResponseStatusCodeSame(401);
        $this->assertJsonContains(['status' => 401]);
        $this->assertJsonContains(['detail' => 'Informations d\'identification non valides.']);
    }
}
