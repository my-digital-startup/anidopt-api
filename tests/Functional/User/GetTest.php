<?php

namespace App\Tests\Functional\User;

use App\Entity\User\User;
use App\Repository\User\UserRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class GetTest extends AbstractTest
{
    private EntityManager $entityManager;

    private UserRepository $userRepository;

    private User $user;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->userRepository = $this->entityManager
            ->getRepository(User::class);

        $this->user = $this->userRepository->findAll()[6];
    }

    /**
     * GET /users
     *
     * @return void
     */
    public function testGetUsers()
    {
        $response = $this->createClient()->request('GET', '/api/v1/users');

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/User']);
        $this->assertJsonContains(['@id' => '/api/v1/users']);
        $this->assertJsonContains(['@type' => 'hydra:Collection']);
    }

    /**
     * GET /users/{uuid}
     *
     * @return void
     */
    public function testGetUser()
    {
        $response = $this->createClientWithCredentials()->request('GET', '/api/v1/users/' . $this->user->getUuid());

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Adopter']);
        $this->assertJsonContains(['@id' => '/api/v1/adopters/' . $data->uuid]);
        $this->assertJsonContains(['@type' => 'Adopter']);
        $this->assertJsonContains(['uuid' => $data->uuid]);
        $this->assertJsonContains(['firstName' => $data->firstName]);
        $this->assertJsonContains(['lastName' => $data->lastName]);
        $this->assertJsonContains(['pseudo' => $data->pseudo]);
        $this->assertJsonContains(['email' => $data->email]);
        $this->assertJsonContains(['roles' => $data->roles]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
        $this->assertJsonContains(['account' => get_object_vars($data->account)]);
    }

    /**
     * GET /users/{uuid} Without be logged in
     *
     * @return void
     */
    public function testGetUserWithoutBeLoggedIn()
    {
        $response = $this->createClient()->request('GET', '/api/v1/users/' . $this->user->getUuid());
        $this->assertResponseStatusCodeSame(401);
        $this->assertJsonContains(['status' => 401]);
        $this->assertJsonContains(['detail' => 'L\'élément nécéssaire à la connexion n\'a pas été trouvé. Veuillez réessayer.']);
    }

    /**
     * GET /users/{uuid} Accès refusé.
     *
     * @return void
     */
    public function testGetUserAccessDenied()
    {
        $user = $this->userRepository->findAll()[1];
        $this->createClientWithCredentials()->request('GET', '/api/v1/users/' . $user->getUuid());

        $this->assertResponseStatusCodeSame(403);
        $this->assertJsonContains(['status' => 403]);
        $this->assertJsonContains(['detail' => 'Accès refusé.']);
    }

    /**
     * GET /users/{uuid} Ressource non trouvée.
     *
     * @return void
     */
    public function testGetUserNotFound()
    {
        $this->createClientWithCredentials()->request('GET', '/api/v1/users/' . $this->user->getUuid() . 'a');

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }

    /**
     * Get /users/me
     *
     * @return void
     */
    public function testGetMe()
    {
        $response = $this->createClientWithCredentials()->request('GET', '/api/v1/users/me');

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['uuid' => $data->uuid]);
        $this->assertJsonContains(['firstName' => $data->firstName]);
        $this->assertJsonContains(['lastName' => $data->lastName]);
        $this->assertJsonContains(['pseudo' => $data->pseudo]);
        $this->assertJsonContains(['email' => $data->email]);
        $this->assertJsonContains(['roles' => $data->roles]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
        $this->assertJsonContains(['account' => get_object_vars($data->account)]);
        $this->assertTrue($data->account->activationToken === null);
        $this->assertTrue($data->account->isVerified);
    }
}
