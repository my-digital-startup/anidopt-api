<?php

namespace App\Tests\Functional\MediaObject;

use App\Tests\Functional\AbstractTest;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PostTest extends AbstractTest
{
    /**
     * POST /media_objects
     *
     * @return void
     */
    public function testPostMediaObject(): void
    {
        copy(__DIR__ . '/Ressources/placeholder.jpeg', __DIR__ . '/Ressources/placeholder-copy.jpeg');
        $src = (__DIR__ . '/Ressources/placeholder-copy.jpeg');

        $file = new UploadedFile($src, 'placeholder.jpeg', 'application/octet-stream', null, true);

        $response = $this->createClientWithCredentials('shelter')->request('POST', '/api/v1/media_objects', [
            'headers' => ['Content-Type' => 'multipart/form-data'],
            'extra' => [
                'files' => [
                    'file' => $file,
                ],
            ]
        ]);

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(201);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/MediaObject']);
        $this->assertJsonContains(['@id' => '/api/v1/media_objects/2']);
        $this->assertJsonContains(['@type' => 'MediaObject']);
        $this->assertJsonContains(['contentUrl' => $data->contentUrl]);
        $this->assertJsonContains(['pet' => $data->pet]);
        $this->assertJsonContains(['author' => $data->author]);
    }

    /**
     * POST /media_objects wrong extension
     *
     * @return void
     */
    public function testPostMediaObjectWithWrongExtension()
    {
        copy(__DIR__ . '/Ressources/file.txt', __DIR__ . '/Ressources/file-copy.txt');
        $src = (__DIR__ . '/Ressources/file-copy.txt');

        $file = new UploadedFile($src, 'file.text', 'application/octet-stream', null, true);

        $this->createClientWithCredentials('shelter')->request('POST', '/api/v1/media_objects', [
            'headers' => ['Content-Type' => 'multipart/form-data'],
            'extra' => [
                'files' => [
                    'file' => $file,
                ],
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'filePath: Seules les extensions suivantes sont autorisées (.jpeg, .jpg, .png, .webp)'
        ]]);
    }
}
