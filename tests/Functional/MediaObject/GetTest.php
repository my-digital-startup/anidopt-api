<?php

namespace App\Tests\Functional\MediaObject;

use App\Entity\MediaObject;
use App\Repository\MediaObjectRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class GetTest extends AbstractTest
{
    /**
     * GET /media_objects
     *
     * @return void
     */
    public function testGetMediaObjects()
    {
        $response = $this->createClient()->request('GET', '/api/v1/media_objects');

        $data = json_decode($response->getContent());
        $hydraMember = $data->{'hydra:member'};
        $hydraTotalItems = $data->{'hydra:totalItems'};

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/MediaObject']);
        $this->assertJsonContains(['@id' => '/api/v1/media_objects']);
        $this->assertJsonContains(['@type' => 'hydra:Collection']);
        $this->assertTrue($hydraMember !== null);
        $this->assertTrue($hydraTotalItems !== null);
    }

    /**
     * GET /media_objects/{id}
     *
     * @return void
     */
    public function testGetMediaObject()
    {
        $response = $this->createClientWithCredentials()->request('GET', '/api/v1/media_objects/1');

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/MediaObject']);
        $this->assertJsonContains(['@id' => '/api/v1/media_objects/1']);
        $this->assertJsonContains(['@type' => 'MediaObject']);
        $this->assertJsonContains(['contentUrl' => $data->contentUrl]);
        $this->assertJsonContains(['pet' => $data->pet]);
        $this->assertJsonContains(['author' => $data->author]);
    }

    /**
     * GET /media_objects/{id} not found
     *
     * @return void
     */
    public function testGetMediaObjectNotFound()
    {
        $this->createClientWithCredentials()->request('GET', '/api/v1/media_objects/1001');

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
