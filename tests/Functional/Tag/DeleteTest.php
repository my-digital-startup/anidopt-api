<?php

namespace App\Tests\Functional\Tag;

use App\Tests\Functional\AbstractTest;

class DeleteTest extends AbstractTest
{
    /**
     * DELETE /tags/{id}
     *
     * @return void
     */
    public function testDeleteTag()
    {
        $this->createClient()->request('DELETE', '/api/v1/tags/3');
        $this->assertResponseStatusCodeSame(204);
    }

    /**
     * DELETE /tags/{id} if breed not found
     *
     * @return void
     */
    public function testDeleteTagNotFound()
    {
        $this->createClient()->request('DELETE', '/api/v1/tags/3');

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
