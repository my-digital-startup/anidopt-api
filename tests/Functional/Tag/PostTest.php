<?php

namespace App\Tests\Functional\Tag;

use App\Entity\Pet\Tag;
use App\Repository\Pet\TagRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class PostTest extends AbstractTest
{
    private EntityManager $entityManager;

    private TagRepository $tagRepository;

    private Tag $tag;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->tagRepository = $this->entityManager
            ->getRepository(Tag::class);

        $this->tag = $this->tagRepository->findOneBy([]);
    }

    /**
     * POST /tags
     *
     * @return void
     */
    public function testPostTag()
    {
        $response = $this->createClient()->request('POST', '/api/v1/tags', [
            'json' => [
                'name' => 'TAG test'
            ]
        ]);

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(201);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Tag']);
        $this->assertJsonContains(['@id' => '/api/v1/tags/' . $data->id]);
        $this->assertJsonContains(['@type' => 'Tag']);
        $this->assertJsonContains(['id' => $data->id]);
        $this->assertJsonContains(['name' => $data->name]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
    }

    /**
     * POST /tags With existing name
     *
     * @return void
     */
    public function testPostTagWithAnExistingName()
    {
        $this->createClient()->request('POST', '/api/v1/tags', [
            'json' => [
                'name' => 'TAG test'
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'name: Cette valeur est déjà utilisée.',
        ]]);
    }

    /**
     * POST /tags Without Data
     *
     * @return void
     */
    public function testPostTagWithoutData()
    {
        $this->createClient()->request('POST', '/api/v1/tags', ['json' => []]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'name: Cette valeur ne doit pas être vide.',
        ]]);
    }
}
