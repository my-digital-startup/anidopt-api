<?php

namespace App\Tests\Functional\Tag;

use App\Entity\Pet\Tag;
use App\Repository\Pet\TagRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class GetTest extends AbstractTest
{
    private EntityManager $entityManager;

    private TagRepository $tagsRepository;

    private Tag $tag;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->tagsRepository = $this->entityManager
            ->getRepository(Tag::class);

        $this->tag = $this->tagsRepository->findOneBy([]);
    }

    /**
     * GET /tags
     *
     * @return void
     */
    public function testGetTags()
    {
        $response = $this->createClient()->request('GET', '/api/v1/tags');

        $data = json_decode($response->getContent());
        $hydraMember = $data->{'hydra:member'};
        $hydraTotalItems = $data->{'hydra:totalItems'};

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Tag']);
        $this->assertJsonContains(['@id' => '/api/v1/tags']);
        $this->assertJsonContains(['@type' => 'hydra:Collection']);
        $this->assertTrue($hydraMember !== null);
        $this->assertTrue($hydraTotalItems !== null);
    }

    /**
     * GET /tags/{uuid}
     *
     * @return void
     */
    public function testGetTag()
    {
        $response = $this->createClientWithCredentials()->request('GET', '/api/v1/tags/' . $this->tag->getId());

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Tag']);
        $this->assertJsonContains(['@id' => '/api/v1/tags/' . $data->id]);
        $this->assertJsonContains(['@type' => 'Tag']);
        $this->assertJsonContains(['name' => $data->name]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
    }

    /**
     * GET /tags/{uuid} Ressource non trouvée.
     *
     * @return void
     */
    public function testGetSpeciNotFound()
    {
        $this->createClientWithCredentials()->request('GET', '/api/v1/tags/' . ($this->tag->getId()) + 1000);

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
