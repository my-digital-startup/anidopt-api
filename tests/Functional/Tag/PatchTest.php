<?php

namespace App\Tests\Functional\Tag;

use App\Tests\Functional\AbstractTest;

class PatchTest extends AbstractTest
{
    /**
     * PATCH /tags/{id}
     *
     * @return void
     */
    public function testPatchTag()
    {
        $response = $this->createClientWithCredentialsForPathMethod()->request('PATCH', '/api/v1/tags/1', [
                'json' => [
                    'name' => 'TAG PATCH test'
                ],
            ]);

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Tag']);
        $this->assertJsonContains(['@id' => '/api/v1/tags/1']);
        $this->assertJsonContains(['@type' => 'Tag']);
        $this->assertJsonContains(['id' => $data->id]);
        $this->assertJsonContains(['name' => $data->name]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
        $this->assertEquals('TAG PATCH test', $data->name);
    }

    /**
     * PATCH /tags/{id} with an existing name
     *
     * @return void
     */
    public function testPatchTagWithAnExistingName()
    {
        $this->createClientWithCredentialsForPathMethod()->request('PATCH', '/api/v1/tags/2', [
                'json' => [
                    'name' => 'TAG PATCH test'
                ],
            ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'name: Cette valeur est déjà utilisée.'
        ]]);
    }

    /**
     * PATCH /tags/{id} Ressource non trouvée.
     *
     * @return void
     */
    public function testPatchTagNotFound()
    {
        $this->createClientWithCredentialsForPathMethod()->request('PATCH', '/api/v1/tags/1000', [
            'json' => [
                'name' => 'TAG PATCH test 2'
            ],
        ]);

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
