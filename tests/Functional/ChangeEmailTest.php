<?php

namespace App\Tests\Functional;

use App\Entity\User\Adopter;
use App\Entity\User\User;
use App\Repository\User\AdopterRepository;
use App\Repository\User\UserRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class ChangeEmailTest extends AbstractTest
{
    private EntityManager $entityManager;

    private AdopterRepository $adopterRepository;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->adopterRepository = $this->entityManager
            ->getRepository(Adopter::class);
    }

    /**
     * Test change email system
     *
     * @return void
     */
    public function testChangeEmailSystem()
    {
        $user = $this->adopterRepository->findOneBy([]);
        $this->createClientWithCredentialsForPathMethod()->request('PATCH', '/api/v1/users/' . $user->getUuid() . '/email', [
            'json' => [
                'email' => 'adopter@anidopt.bzh',
            ],
        ]);

        $this->assertResponseStatusCodeSame(200);
    }

    /**
     * Test change email system without be logged in
     *
     * @return void
     */
    public function testChangeEmailSystemWithoutBeLoggedIn()
    {
        $user = $this->adopterRepository->findOneBy([]);
        $this->createClient()->request('PATCH', '/api/v1/users/' . $user->getUuid() . '/email', [
            'json' => [
                'email' => 'adopter@anidopt.bzh',
            ],
        ]);

        $this->assertResponseStatusCodeSame(401);
        $this->assertJsonContains(['status' => 401]);
        $this->assertJsonContains(['detail' => 'L\'élément nécéssaire à la connexion n\'a pas été trouvé. Veuillez réessayer.']);
    }

    /**
     * Test change email system if user Ressource non trouvée.
     *
     * @return void
     */
    public function testChangeEmailSystemUserNotFound()
    {
        $user = $this->adopterRepository->findOneBy([]);
        $this->createClient()->request('PATCH', '/api/v1/users/' . $user->getUuid() . 'a/email', [
            'json' => [
                'email' => 'adopter@anidopt.bzh',
            ],
        ]);

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
