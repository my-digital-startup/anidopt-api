<?php

namespace App\Tests\Functional\Shelter;

use App\Entity\User\Shelter;
use App\Repository\User\ShelterRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class PostTest extends AbstractTest
{
    private EntityManager $entityManager;

    private ShelterRepository $shelterRepository;

    private Shelter $shelter;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->shelterRepository = $this->entityManager
            ->getRepository(Shelter::class);

        $this->shelter = $this->shelterRepository->findOneBy([]);
    }

    /**
     * POST /shelters
     *
     * @return void
     */
    public function testPostShelter()
    {
        $response = $this->createClient()->request('POST', '/api/v1/shelters', [
            'json' => [
                'email' => 'shelter-test@anidopt.bzh',
                'password' => 'password',
                'name' => 'Shelter test name'
            ]
        ]);

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(201);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Shelter']);
        $this->assertJsonContains(['@id' => '/api/v1/shelters/' . $data->uuid]);
        $this->assertJsonContains(['@type' => 'Shelter']);
        $this->assertJsonContains(['uuid' => $data->uuid]);
        $this->assertJsonContains(['email' => $data->email]);
        $this->assertJsonContains(['roles' => $data->roles]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
        $this->assertJsonContains(['account' => get_object_vars($data->account)]);
        $this->assertTrue($data->account->activationToken !== null);
        $this->assertTrue(!$data->account->isVerified);
    }

    /**
     * POST /shelters Without Email
     *
     * @return void
     */
    public function testPostShelterWithoutEmail()
    {
        $this->createClient()->request('POST', '/api/v1/shelters', [
            'json' => [
                'password' => 'password',
                'name' => 'Shelter test name +1'
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'email: Le champ email doit être renseigné.'
        ]]);
    }

    /**
     * POST /shelters Without Password
     *
     * @return void
     */
    public function testPostShelterWithoutPassword()
    {
        $this->createClient()->request('POST', '/api/v1/shelters', [
            'json' => [
                'email' => 'shelter-test+1@anidopt.bzh',
                'name' => 'Shelter test name +2'
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'password: Le champ mot de passe doit être renseigné.'
        ]]);
    }

    /**
     * POST /shelters Without Name
     *
     * @return void
     */
    public function testPostShelterWithoutName()
    {
        $this->createClient()->request('POST', '/api/v1/shelters', [
            'json' => [
                'email' => 'shelter-test+1@anidopt.bzh',
                'password' => 'password'
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'name: Le champ nom ne peut pas être vide.'
        ]]);
    }

    /**
     * POST /shelters Without Data
     *
     * @return void
     */
    public function testPostShelterWithoutData()
    {
        $this->createClient()->request('POST', '/api/v1/shelters', ['json' => []]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'name: Le champ nom ne peut pas être vide.',
            1 => 'email: Le champ email doit être renseigné.',
            2 => 'password: Le champ mot de passe doit être renseigné.'
        ]]);
    }
}
