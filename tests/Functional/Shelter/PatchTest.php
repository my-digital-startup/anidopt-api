<?php

namespace App\Tests\Functional\Shelter;

use App\Entity\User\User;
use App\Entity\User\Shelter;
use Doctrine\ORM\EntityManager;
use App\Tests\Functional\AbstractTest;
use App\Repository\User\ShelterRepository;

class PatchTest extends AbstractTest
{
    private EntityManager $entityManager;

    private ShelterRepository $shelterRepository;

    private Shelter $shelter;

    private $userRepository;
    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->shelterRepository = $this->entityManager
            ->getRepository(Shelter::class);

        $this->userRepository = $this->entityManager
            ->getRepository(User::class);

        $this->shelter = $this->shelterRepository->findAll()[0];
    }

    /**
     * PATCH /shelters/{uuid}
     *
     * @return void
     */
    public function testPatchShelter()
    {
        $response = $this->createClientWithCredentialsForPathMethod('shelter')->request(
            'PATCH',
            '/api/v1/shelters/' . $this->shelter->getUuid(),
            [
                'json' => [
                    'name' => 'Shelter name',
                    'account' => [
                        'address' => 'Shelter address',
                        'city' => 'Shelter city',
                        'zipCode' => "123456"
                    ]
                ],
            ]
        );

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['name' => $data->name]);
        $this->assertEquals('Shelter name', $data->name);
    }

    /**
     * PATCH /shelters/{uuid} Accès refusé.
     *
     * @return void
     */
    public function testPatchShelterAccessDenied()
    {
        $shelter = $this->shelterRepository->findAll()[2];
        $this->createClientWithCredentialsForPathMethod()->request(
            'PATCH',
            '/api/v1/shelters/' . $shelter->getUuid(),
            [
                'json' => [
                    'name' => 'Shelter name',
                    'account' => [
                        'address' => 'Shelter address',
                        'city' => 'Shelter city',
                        'zipCode' => "123456"
                    ]
                ],
            ]
        );

        $this->assertResponseStatusCodeSame(403);
        $this->assertJsonContains(['status' => 403]);
        $this->assertJsonContains(['detail' => 'Accès refusé.']);
    }

    /**
     * PATCH /shelters/{uuid} Ressource non trouvée.
     *
     * @return void
     */
    public function testPatchShelterNotFound()
    {
        $this->createClientWithCredentialsForPathMethod()->request(
            'PATCH',
            '/api/v1/shelters/' . $this->shelter->getUuid() . 'a',
            [
                'json' => [
                    'pseudo' => 'Shelter name',
                    'account' => [
                        'address' => 'Shelter address',
                        'city' => 'Shelter city',
                        'zipCode' => "123456"
                    ]
                ],
            ]
        );

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
