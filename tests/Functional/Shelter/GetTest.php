<?php

namespace App\Tests\Functional\Shelter;

use App\Entity\User\Shelter;
use App\Repository\User\ShelterRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class GetTest extends AbstractTest
{
    private EntityManager $entityManager;

    private ShelterRepository $shelterRepository;

    private Shelter $shelter;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->shelterRepository = $this->entityManager
            ->getRepository(Shelter::class);

        $this->shelter = $this->shelterRepository->findOneBy([]);
    }

    /**
     * GET /shelters
     *
     * @return void
     */
    public function testGetShelters()
    {
        $response = $this->createClient()->request('GET', '/api/v1/shelters');

        $data = json_decode($response->getContent());
        $hydraMember = $data->{'hydra:member'};
        $hydraTotalItems = $data->{'hydra:totalItems'};

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Shelter']);
        $this->assertJsonContains(['@id' => '/api/v1/shelters']);
        $this->assertJsonContains(['@type' => 'hydra:Collection']);
        $this->assertTrue($hydraMember !== null);
        $this->assertTrue($hydraTotalItems !== null);
    }

    /**
     * GET /shelters/{uuid}
     *
     * @return void
     */
    public function testGetShelter()
    {
        $response = $this->createClientWithCredentials('shelter')->request('GET', '/api/v1/shelters/' . $this->shelter->getUuid());

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Shelter']);
        $this->assertJsonContains(['@id' => '/api/v1/shelters/' . $data->uuid]);
        $this->assertJsonContains(['@type' => 'Shelter']);
        $this->assertJsonContains(['uuid' => $data->uuid]);
        $this->assertJsonContains(['name' => $data->name]);
        $this->assertJsonContains(['email' => $data->email]);
        $this->assertJsonContains(['roles' => $data->roles]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
        $this->assertJsonContains(['account' => get_object_vars($data->account)]);
    }

    /**
     * GET /shelters/{uuid} Ressource non trouvée.
     *
     * @return void
     */
    public function testGetShelterNotFound()
    {
        $this->createClientWithCredentials('shelter')->request('GET', '/api/v1/shelters/' . $this->shelter->getUuid() . 'a');

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
