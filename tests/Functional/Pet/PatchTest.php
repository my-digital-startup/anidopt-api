<?php

namespace App\Tests\Functional\Pet;

use App\Entity\Pet\Pet;
use App\Entity\User\Shelter;
use Doctrine\ORM\EntityManager;
use App\Repository\Pet\PetRepository;
use App\Tests\Functional\AbstractTest;
use App\Repository\User\ShelterRepository;

class PatchTest extends AbstractTest
{
    private EntityManager $entityManager;

    private PetRepository $petRepository;

    private ShelterRepository $shelterRepository;

    private Pet $pet;

    private Shelter $shelter;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->petRepository = $this->entityManager
            ->getRepository(Pet::class);

        $this->shelterRepository = $this->entityManager
            ->getRepository(Shelter::class);

        $this->pet = $this->petRepository->findOneBy([]);

        $this->shelter = $this->shelterRepository->findAll()[1];
    }

    /**
     * PATCH /pets/{uuid}
     *
     * @return void
     */
    public function testPatchPet()
    {
        $response = $this->createClientWithCredentialsForPathMethod('shelter')->request(
            'PATCH',
            '/api/v1/pets/' . $this->pet->getUuid(),
            [
                'json' => [
                    'name' => 'PET NAME test',
                    'description' => 'PET DESCRIPTION test',
                    'isMale' => true,
                    'birthday' => '2020-07-06T13:51:57+00:00',
                    'isSterilised' => true,
                    'identificationNumber' => 'azertyuio1234567890',
                    'specie' => '/api/v1/species/2',
                    'breeds' => [],
                    'tags' => []
                ],
            ]
        );

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['name' => $data->name]);
        $this->assertJsonContains(['description' => $data->description]);
        $this->assertJsonContains(['isMale' => $data->isMale]);
        $this->assertJsonContains(['birthday' => $data->birthday]);
        $this->assertJsonContains(['isMale' => $data->isSterilised]);
        $this->assertJsonContains(['identificationNumber' => $data->identificationNumber]);
        $this->assertJsonContains(['breeds' => $data->breeds]);
        $this->assertJsonContains(['tags' => $data->tags]);

        $this->assertEquals('PET NAME test', $data->name);
        $this->assertEquals('PET DESCRIPTION test', $data->description);
        $this->assertEquals(true, $data->isMale);
        $this->assertEquals('2020-07-06T13:51:57+00:00', $data->birthday);
        $this->assertEquals(true, $data->isSterilised);
        $this->assertEquals('azertyuio1234567890', $data->identificationNumber);
        $this->assertEquals([], $data->breeds);
        $this->assertEquals([], $data->tags);
    }

    /**
     * PATCH /pets/{uuid} as an adopter
     *
     * @return void
     */
    public function testPatchPetAsAnAdopter()
    {
        $this->createClientWithCredentialsForPathMethod('adopter')->request(
            'PATCH',
            '/api/v1/pets/' . $this->pet->getUuid(),
            [
                'json' => [
                    'name' => 'PET NAME test',
                    'description' => 'PET DESCRIPTION test',
                    'isMale' => true,
                    'birthday' => '2020-07-06T13:51:57+00:00',
                    'isSterilised' => true,
                    'identificationNumber' => 'azertyuio1234567890',
                    'specie' => '/api/v1/species/2',
                    'breeds' => [],
                    'tags' => []
                ],
            ]
        );

        $this->assertResponseStatusCodeSame(403);
        $this->assertJsonContains(['status' => 403]);
        $this->assertJsonContains(['detail' => 'Accès refusé.']);
    }

    /**
     * PATCH /pets/{uuid} as a random shelter
     *
     * @return void
     */
    public function testPatchPetAsARandomShelter()
    {
        $this->shelter->getAccount()->setIsVerified(true);
        $this->entityManager->flush();

        $response = $this->createClient()->request('POST', '/api/v1/login_check', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'username' => $this->shelter->getEmail(),
                'password' => 'password'
            ],
        ]);

        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $token = $data->token;

        $this->createClient()->request(
            'PATCH',
            '/api/v1/pets/' . $this->pet->getUuid(),
            [
                'headers' => [
                    'authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/merge-patch+json'
                ],
                'json' => [
                    'name' => 'PET NAME test',
                    'description' => 'PET DESCRIPTION test',
                    'isMale' => true,
                    'birthday' => '2020-07-06T13:51:57+00:00',
                    'isSterilised' => true,
                    'identificationNumber' => 'azertyuio1234567890',
                    'specie' => '/api/v1/species/2',
                    'breeds' => [],
                    'tags' => []
                ],
            ]
        );

        $this->assertResponseStatusCodeSame(403);
        $this->assertJsonContains(['status' => 403]);
        $this->assertJsonContains(['detail' => 'Accès refusé.']);
    }

    /**
     * PATCH /pets/{uuid} Ressource non trouvée.
     *
     * @return void
     */
    public function testPatchPetNotFound()
    {
        $this->createClientWithCredentialsForPathMethod()->request(
            'PATCH',
            '/api/v1/pets/' . $this->pet->getUuid() . 'aoijoie',
            [
                'json' => [
                    'name' => 'PET NAME test',
                    'description' => 'PET DESCRIPTION test',
                    'isMale' => true,
                    'birthday' => '2020-07-06T13:51:57+00:00',
                    'isSterilised' => true,
                    'identificationNumber' => 'azertyuio1234567890',
                    'specie' => '/api/v1/species/2',
                    'breeds' => [],
                    'tags' => []
                ],
            ]
        );

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
