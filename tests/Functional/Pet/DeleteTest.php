<?php

namespace App\Tests\Functional\Pet;

use App\Entity\Pet\Pet;
use App\Entity\User\Shelter;
use Doctrine\ORM\EntityManager;
use App\Repository\Pet\PetRepository;
use App\Tests\Functional\AbstractTest;
use App\Repository\User\ShelterRepository;

class DeleteTest extends AbstractTest
{
    private EntityManager $entityManager;

    private PetRepository $petRepository;

    private Pet $pet;

    private ShelterRepository $shelterRepository;

    private Shelter $shelter;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->petRepository = $this->entityManager
            ->getRepository(Pet::class);

        $this->pet = $this->petRepository->findOneBy([]);

        $this->shelterRepository = $this->entityManager
            ->getRepository(Shelter::class);

        $this->shelter = $this->shelterRepository->findAll()[1];
    }

    /**
     * DELETE /pets/{uuid}
     *
     * @return void
     */
    public function testDeletePet()
    {
        $this->createClientWithCredentials('shelter')->request(
            'DELETE',
            '/api/v1/pets/' . $this->pet->getUuid()
        );

        $this->assertResponseStatusCodeSame(204);
    }

    /**
     * DELETE /pets/{uuid} as an adopter
     *
     * @return void
     */
    public function testDeletePetAsAnAdopter()
    {
        $this->createClientWithCredentials('adopter')->request(
            'DELETE',
            '/api/v1/pets/' . $this->pet->getUuid()
        );

        $this->assertResponseStatusCodeSame(403);
        $this->assertJsonContains(['status' => 403]);
        $this->assertJsonContains(['detail' => 'Accès refusé.']);
    }

    /**
     * DELETE /pets/{uuid} as a random shelter
     *
     * @return void
     */
    public function testDeletePetAsARandomShelter()
    {
        $this->shelter->getAccount()->setIsVerified(true);
        $this->entityManager->flush();

        $response = $this->createClient()->request('POST', '/api/v1/login_check', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'username' => $this->shelter->getEmail(),
                'password' => 'password'
            ],
        ]);

        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $token = $data->token;

        $this->createClient()->request(
            'DELETE',
            '/api/v1/pets/' . $this->pet->getUuid(),
            [
                'headers' => [
                    'authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/merge-patch+json'
                ],
            ]
        );

        $this->assertResponseStatusCodeSame(403);
        $this->assertJsonContains(['status' => 403]);
        $this->assertJsonContains(['detail' => 'Accès refusé.']);
    }

    /**
     * DELETE /pets/{uuid} not found.
     *
     * @return void
     */
    public function testDeletePetNotFound()
    {
        $this->createClientWithCredentials()->request(
            'DELETE',
            '/api/v1/pets/' . $this->pet->getUuid() . 'aoijoie',
            [
                'json' => [
                    'name' => 'PET NAME test',
                    'description' => 'PET DESCRIPTION test',
                    'isMale' => true,
                    'birthday' => '2020-07-06T13:51:57+00:00',
                    'isSterilised' => true,
                    'identificationNumber' => 'azertyuio1234567890',
                    'specie' => '/api/v1/species/2',
                    'breeds' => [],
                    'tags' => []
                ],
            ]
        );

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
