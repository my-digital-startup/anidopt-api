<?php

namespace App\Tests\Functional\Pet;

use App\Entity\Pet\Pet;
use App\Repository\Pet\PetRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class GetTest extends AbstractTest
{
    private EntityManager $entityManager;

    private PetRepository $petRepository;

    private Pet $pet;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->petRepository = $this->entityManager
            ->getRepository(Pet::class);

        $this->pet = $this->petRepository->findOneBy([]);
    }

    /**
     * GET /pets
     *
     * @return void
     */
    public function testGetPets()
    {
        $response = $this->createClient()->request('GET', '/api/v1/pets');

        $data = json_decode($response->getContent());
        $hydraMember = $data->{'hydra:member'};
        $hydraTotalItems = $data->{'hydra:totalItems'};

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Pet']);
        $this->assertJsonContains(['@id' => '/api/v1/pets']);
        $this->assertJsonContains(['@type' => 'hydra:Collection']);
        $this->assertTrue($hydraMember !== null);
        $this->assertTrue($hydraTotalItems !== null);
    }

    /**
     * GET /pets/{uuid}
     *
     * @return void
     */
    public function testGetPet()
    {
        $response = $this->createClientWithCredentials()->request('GET', '/api/v1/pets/' . $this->pet->getUuid());

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Pet']);
        $this->assertJsonContains(['@id' => '/api/v1/pets/' . $data->uuid]);
        $this->assertJsonContains(['@type' => 'Pet']);
        $this->assertJsonContains(['uuid' => $data->uuid]);
        $this->assertJsonContains(['name' => $data->name]);
        $this->assertJsonContains(['slug' => $data->slug]);
        $this->assertJsonContains(['description' => $data->description]);
        $this->assertJsonContains(['isMale' => $data->isMale]);
        $this->assertJsonContains(['birthday' => $data->birthday]);
        $this->assertJsonContains(['isSterilised' => $data->isSterilised]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
    }

    /**
     * GET /pets/{uuid} Ressource non trouvée.
     *
     * @return void
     */
    public function testGetPetNotFound()
    {
        $this->createClientWithCredentials()->request('GET', '/api/v1/pets/' . $this->pet->getUuid() . 'a');

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
