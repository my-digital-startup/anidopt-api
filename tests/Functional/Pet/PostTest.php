<?php

namespace App\Tests\Functional\Pet;

use DateTimeImmutable;
use App\Entity\Pet\Pet;
use App\Entity\User\Shelter;
use Doctrine\ORM\EntityManager;
use App\Repository\Pet\PetRepository;
use App\Tests\Functional\AbstractTest;
use App\Repository\User\ShelterRepository;

class PostTest extends AbstractTest
{
    private EntityManager $entityManager;

    private PetRepository $petRepository;

    private Pet $pet;

    private ShelterRepository $shelterRepository;

    private Shelter $shelter;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->petRepository = $this->entityManager
            ->getRepository(Pet::class);

        $this->pet = $this->petRepository->findOneBy([]);

        $this->shelterRepository = $this->entityManager
            ->getRepository(Shelter::class);

        $this->shelter = $this->shelterRepository->findOneBy([]);
    }

    /**
     * POST /pets
     *
     * @return void
     */
    public function testPostPet()
    {
        $response = $this->createClientWithCredentials('shelter')->request('POST', '/api/v1/pets', [
            'json' => [
                'name' => 'PET test',
                'description' => 'DESCRIPTION test',
                'isMale' => true,
                'isSterilised' => true,
                'identificationNumber' => 'AZERTYUIOP1234567890',
                'isSos' => true,
                'shelter' => '/api/v1/shelters/' . $this->shelter->getUuid(),
                'specie' => '/api/v1/species/1',
                'breeds' => [
                    '/api/v1/breeds/1', '/api/v1/breeds/2'
                ],
                'tags' => [
                    '/api/v1/tags/1', '/api/v1/tags/2'
                ],
            ]
        ]);

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(201);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Pet']);
        $this->assertJsonContains(['@id' => '/api/v1/pets/' . $data->uuid]);
        $this->assertJsonContains(['@type' => 'Pet']);
        $this->assertJsonContains(['uuid' => $data->uuid]);
        $this->assertJsonContains(['name' => $data->name]);
        $this->assertJsonContains(['slug' => $data->slug]);
        $this->assertJsonContains(['description' => $data->description]);
        $this->assertJsonContains(['isMale' => $data->isMale]);
        $this->assertJsonContains(['isSterilised' => $data->isSterilised]);
        $this->assertJsonContains(['identificationNumber' => $data->identificationNumber]);
        $this->assertJsonContains(['isSos' => $data->isSos]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
    }

    /**
     * POST /shelters Without be a Shelter
     *
     * @return void
     */
    public function testPostPetWithoutBeAShelter()
    {
        $this->createClientWithCredentials()->request('POST', '/api/v1/pets', [
            'json' => [
                'name' => 'PET test',
                'description' => 'DESCRIPTION test',
                'isMale' => true,
                'isSterilised' => true,
                'identificationNumber' => 'AZERTYUIOP12345678901',
                'isSos' => true,
                'shelter' => '/api/v1/shelters/' . $this->shelter->getUuid(),
                'specie' => '/api/v1/species/1',
                'breeds' => [
                    '/api/v1/breeds/1', '/api/v1/breeds/2'
                ],
                'tags' => [
                    '/api/v1/tags/1', '/api/v1/tags/2'
                ],
            ]
        ]);

        $this->assertResponseStatusCodeSame(403);
        $this->assertJsonContains(['status' => 403]);
        $this->assertJsonContains(['detail' => 'Accès refusé.']);
    }

    /**
     * POST /shelters Without Data
     *
     * @return void
     */
    public function testPostShelterWithoutData()
    {
        $this->createClientWithCredentials()->request('POST', '/api/v1/pets', ['json' => []]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'name: Cette valeur ne doit pas être vide.',
            1 => 'isMale: Cette valeur ne doit pas être nulle.',
            2 => 'isSos: Cette valeur ne doit pas être nulle.',
            3 => 'shelter: Cette valeur ne doit pas être nulle.',
            4 => 'specie: Cette valeur ne doit pas être nulle.'
        ]]);
    }
}
