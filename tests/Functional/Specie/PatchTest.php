<?php

namespace App\Tests\Functional\Specie;

use App\Tests\Functional\AbstractTest;

class PatchTest extends AbstractTest
{
    /**
     * PATCH /species/{id}
     *
     * @return void
     */
    public function testPatchSpecie()
    {
        $response = $this->createClientWithCredentialsForPathMethod()->request('PATCH', '/api/v1/species/1', [
                'json' => [
                    'name' => 'SPECIE PATCH test'
                ],
            ]);

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Specie']);
        $this->assertJsonContains(['@id' => '/api/v1/species/1']);
        $this->assertJsonContains(['@type' => 'Specie']);
        $this->assertJsonContains(['id' => $data->id]);
        $this->assertJsonContains(['name' => $data->name]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
        $this->assertEquals('SPECIE PATCH test', $data->name);
    }

    /**
     * PATCH /species/{id} with an existing name
     *
     * @return void
     */
    public function testPatchSpecieWithAnExistingName()
    {
        $this->createClientWithCredentialsForPathMethod()->request('PATCH', '/api/v1/species/2', [
                'json' => [
                    'name' => 'SPECIE PATCH test'
                ],
            ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'name: Cette valeur est déjà utilisée.'
        ]]);
    }

    /**
     * PATCH /species/{id} Ressource non trouvée.
     *
     * @return void
     */
    public function testPatchSpecieNotFound()
    {
        $this->createClientWithCredentialsForPathMethod()->request('PATCH', '/api/v1/species/1000', [
            'json' => [
                'name' => 'SPECIE PATCH test 2'
            ],
        ]);

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
