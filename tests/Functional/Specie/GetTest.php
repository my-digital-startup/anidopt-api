<?php

namespace App\Tests\Functional\Specie;

use App\Entity\Pet\Specie;
use App\Repository\Pet\SpecieRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class GetTest extends AbstractTest
{
    private EntityManager $entityManager;

    private SpecieRepository $speciesRepository;

    private Specie $specie;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->speciesRepository = $this->entityManager
            ->getRepository(Specie::class);

        $this->specie = $this->speciesRepository->findOneBy([]);
    }

    /**
     * GET /species
     *
     * @return void
     */
    public function testGetSpecies()
    {
        $response = $this->createClient()->request('GET', '/api/v1/species');

        $data = json_decode($response->getContent());
        $hydraMember = $data->{'hydra:member'};
        $hydraTotalItems = $data->{'hydra:totalItems'};

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Specie']);
        $this->assertJsonContains(['@id' => '/api/v1/species']);
        $this->assertJsonContains(['@type' => 'hydra:Collection']);
        $this->assertTrue($hydraMember !== null);
        $this->assertTrue($hydraTotalItems !== null);
    }

    /**
     * GET /species/{uuid}
     *
     * @return void
     */
    public function testGetSpecie()
    {
        $response = $this->createClientWithCredentials()->request('GET', '/api/v1/species/' . $this->specie->getId());

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Specie']);
        $this->assertJsonContains(['@id' => '/api/v1/species/' . $data->id]);
        $this->assertJsonContains(['@type' => 'Specie']);
        $this->assertJsonContains(['name' => $data->name]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
    }

    /**
     * GET /species/{uuid} Ressource non trouvée.
     *
     * @return void
     */
    public function testGetSpecieNotFound()
    {
        $this->createClientWithCredentials()->request('GET', '/api/v1/species/' . ($this->specie->getId()) + 1000);

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
