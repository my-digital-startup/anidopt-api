<?php

namespace App\Tests\Functional\Specie;

use App\Entity\Pet\Specie;
use App\Repository\Pet\SpecieRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class PostTest extends AbstractTest
{
    private EntityManager $entityManager;

    private SpecieRepository $specieRepository;

    private Specie $specie;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->specieRepository = $this->entityManager
            ->getRepository(Specie::class);

        $this->specie = $this->specieRepository->findOneBy([]);
    }

    /**
     * POST /species
     *
     * @return void
     */
    public function testPostSpecie()
    {
        $response = $this->createClient()->request('POST', '/api/v1/species', [
            'json' => [
                'name' => 'SPECIE test'
            ]
        ]);

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(201);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Specie']);
        $this->assertJsonContains(['@id' => '/api/v1/species/' . $data->id]);
        $this->assertJsonContains(['@type' => 'Specie']);
        $this->assertJsonContains(['id' => $data->id]);
        $this->assertJsonContains(['name' => $data->name]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
    }

    /**
     * POST /species With existing name
     *
     * @return void
     */
    public function testPostSpecieWithAnExistingName()
    {
        $this->createClient()->request('POST', '/api/v1/species', [
            'json' => [
                'name' => 'SPECIE test'
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'name: Cette valeur est déjà utilisée.',
        ]]);
    }

    /**
     * POST /species Without Data
     *
     * @return void
     */
    public function testPostSpecieWithoutData()
    {
        $this->createClient()->request('POST', '/api/v1/species', ['json' => []]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'name: Cette valeur ne doit pas être vide.',
        ]]);
    }
}
