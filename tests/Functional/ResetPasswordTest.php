<?php

namespace App\Tests\Functional;

use App\Entity\User\User;
use Doctrine\ORM\EntityManager;
use App\Tests\Functional\AbstractTest;
use App\Repository\User\UserRepository;

class ResetPasswordTest extends AbstractTest
{
    private EntityManager $entityManager;

    private UserRepository $userRepository;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->userRepository = $this->entityManager
            ->getRepository(User::class);
    }

    /**
     * Test good reset password system
     *
     * @return void
     */
    public function testSuccessfulResetPassword()
    {
        /* First step */
        $this->createClientWithCredentials()->request('PUT', '/api/v1/users/password/reset', [
            'json' => ['email' => 'adopter@anidopt.bzh'],
        ]);

        $token = $this->userRepository->findOneBy(['email' => 'adopter@anidopt.bzh'])
            ->getAccount()
            ->getResetPasswordToken();

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['detail' => 'L\'email a bien été envoyé. Veuillez consulter votre messagerie pour poursuivre la démarche de réinitialisation de mot de passe.']);

        /* Second step */
        $this->createClientWithCredentials()->request('PUT', '/api/v1/users/password/reset', [
            'json' => [
                'email' => 'adopter@anidopt.bzh',
                'token' => $token,
                'password' => 'password'
            ],
        ]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['detail' => 'Le mot de passe a bien été réinitialisé.']);
    }

    /**
     * Test the reset password system without email
     *
     * @return void
     */
    public function testResetPasswordWihtoutEmail()
    {
        $this->createClient()->request('PUT', '/api/v1/users/password/reset', [
            'json' => [],
        ]);

        $this->assertResponseStatusCodeSame(500);
        $this->assertJsonContains(['detail' => 'Une erreur est survenue.']);
    }

    /**
     * Test the reset password system with an empty token
     *
     * @return void
     */
    public function testResetPasswordWithAnEmptyToken()
    {
        /* First step */
        $this->createClient()->request('PUT', '/api/v1/users/password/reset', [
            'json' => ['email' => 'adopter@anidopt.bzh'],
        ]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['detail' => 'L\'email a bien été envoyé. Veuillez consulter votre messagerie pour poursuivre la démarche de réinitialisation de mot de passe.']);

        /* Second step */
        $this->createClient()->request('PUT', '/api/v1/users/password/reset', [
            'json' => [
                'email' => 'adopter@anidopt.bzh',
                'token' => '',
            ],
        ]);

        $this->assertResponseStatusCodeSame(500);
        $this->assertJsonContains(['detail' => 'Une erreur est survenue.']);
    }

    /**
     * Test the reset password system without a password
     *
     * @return void
     */
    public function testResetPasswordWithoutPassword()
    {
        /* First step */
        $this->createClient()->request('PUT', '/api/v1/users/password/reset', [
            'json' => ['email' => 'adopter@anidopt.bzh'],
        ]);

        $token = $this->userRepository->findOneBy(['email' => 'adopter@anidopt.bzh'])
            ->getAccount()
            ->getResetPasswordToken();

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['detail' => 'L\'email a bien été envoyé. Veuillez consulter votre messagerie pour poursuivre la démarche de réinitialisation de mot de passe.']);

        /* Second step */
        $this->createClient()->request('PUT', '/api/v1/users/password/reset', [
            'json' => [
                'email' => 'adopter@anidopzt.bzh',
                'token' => $token,
            ],
        ]);

        $this->assertResponseStatusCodeSame(500);
        $this->assertJsonContains(['detail' => 'Une erreur est survenue.']);
    }

    /**
     * Test if the user if found or not
     *
     * @return void
     */
    public function testUserNotFound()
    {
        /* First step */
        $this->createClient()->request('PUT', '/api/v1/users/password/reset', [
            'json' => ['email' => 'adoptr@anidopt.bzh'],
        ]);

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['detail' => 'L\'utilisateur n\'a pas été trouvé.']);
    }
}
