<?php

namespace App\Tests\Functional;

use App\Entity\User\Adopter;
use App\Repository\User\AdopterRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class ChangePasswordTest extends AbstractTest
{
    private EntityManager $entityManager;

    private AdopterRepository $adopterRepository;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->adopterRepository = $this->entityManager
            ->getRepository(Adopter::class);
    }

    /**
     * Test change password system
     *
     * @return void
     */
    public function testSuccessfullChangePasswordSystem()
    {
        $user = $this->adopterRepository->findOneBy([]);
        $this->createClientWithCredentialsForPathMethod()->request('PATCH', '/api/v1/users/' . $user->getUuid() . '/password', [
            'json' => [
                'oldPassword' => 'password',
                'newPassword' => 'password'
            ],
        ]);

        $this->assertResponseStatusCodeSame(200);
    }

    /**
     * Test change password system without be logged in
     *
     * @return void
     */
    public function testChangeEmailSystemWithoutBeLoggedIn()
    {
        $user = $this->adopterRepository->findOneBy([]);
        $this->createClient()->request('PATCH', '/api/v1/users/' . $user->getUuid() . '/email', [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json'
            ],
            'json' => [
                'email' => 'adopter@anidopt.bzh',
            ],
        ]);

        $this->assertResponseStatusCodeSame(401);
        $this->assertJsonContains(['status' => 401]);
        $this->assertJsonContains(['detail' => 'L\'élément nécéssaire à la connexion n\'a pas été trouvé. Veuillez réessayer.']);
    }

    /**
     * Test change password system if user Ressource non trouvée.
     *
     * @return void
     */
    public function testChangeEmailSystemUserNotFound()
    {
        $user = $this->adopterRepository->findOneBy([]);
        $this->createClient()->request('PATCH', '/api/v1/users/' . $user->getUuid() . 'a/email', [
            'json' => [
                'email' => 'adopter@anidopt.bzh',
            ],
        ]);

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
