<?php

namespace App\Tests\Functional;

use App\Entity\User\User;
use App\Repository\User\UserRepository;
use Doctrine\ORM\EntityManager;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelper;

class RegistrationTest extends AbstractTest
{
    private EntityManager $entityManager;

    private UserRepository $userRepository;

    private VerifyEmailHelper $verifyEmailHelper;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->userRepository = $this->entityManager
            ->getRepository(User::class);

        $this->nbUsers = count($this->userRepository->findAll());

        $this->verifyEmailHelper = $kernel->getContainer()->get('app.verify_email_helper');
    }

    /**
     * Test the successfull registration of an adopter
     *
     * @return void
     */
    public function testSuccessfulRegistrationAsAnAdopter()
    {
        /* Checking the addition of the new adopter */
        $response = $this->createClient()->request('POST', '/api/v1/adopters', [
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'email' => 'adopter+1@anidopt.bzh',
                'password' => 'password'
            ]
        ]);

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(201);
        $this->assertEquals($this->nbUsers + 1, count($this->userRepository->findAll()));
        $this->validateJsonResponse($data, 'Adopter', 'adopters');

        /* Checking the user validation system */
        $currentUser = $this->userRepository->findOneBy(['uuid' => $data->uuid]);
        $signatureComponents = $this->verifyEmailHelper->generateSignature(
            'users_verify_account',
            $currentUser->getAccount()->getActivationToken(),
            $currentUser->getEmail(),
            ['activationToken' => $currentUser->getAccount()->getActivationToken()]
        );

        $this->createClient()->request('GET', $signatureComponents->getSignedUrl());

        $this->assertResponseStatusCodeSame(302);
        $this->assertResponseRedirects($_ENV["REGISTER_ACTIVATION_PATH_SUCESSFULL"]);
    }

    /**
     * Test the successfull registration of a shelter
     *
     * @return void
     */
    public function testSuccessfulRegistrationAsAShelter()
    {
        /* Checking the addition of the new shelter */
        $response = $this->createClient()->request('POST', '/api/v1/shelters', [
            'json' => [
                'email' => 'shelter+1@anidopt.bzh',
                'password' => 'password',
                'name' => 'Shelter +1 name'
            ]
        ]);

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(201);
        $this->assertEquals($this->nbUsers + 1, count($this->userRepository->findAll()));
        $this->validateJsonResponse($data, 'Shelter', 'shelters');

        /* Checking the user validation system */
        $currentUser = $this->userRepository->findOneBy(['uuid' => $data->uuid]);
        $signatureComponents = $this->verifyEmailHelper->generateSignature(
            'users_verify_account',
            $currentUser->getAccount()->getActivationToken(),
            $currentUser->getEmail(),
            ['activationToken' => $currentUser->getAccount()->getActivationToken()]
        );

        $this->createClient()->request('GET', $signatureComponents->getSignedUrl());

        $this->assertResponseStatusCodeSame(302);
        $this->assertResponseRedirects($_ENV["REGISTER_ACTIVATION_PATH_SUCESSFULL"]);
    }

    /**
     * Test a registration with an existing email
     *
     * @return void
     */
    public function testRegistrationWithAnExistingEmail()
    {
        $this->createClient()->request('POST', '/api/v1/adopters', [
            'json' => [
                'email' => 'adopter+1@anidopt.bzh',
                'password' => 'password'
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'email: Cet email est déjà utilisé au sein de l\'application.'
        ]]);
    }

    /**
     * Test a registration without email
     *
     * @return void
     */
    public function testRegistrationWithoutEmail()
    {
        $this->createClient()->request('POST', '/api/v1/adopters', [
            'json' => [
                'password' => 'password'
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'email: Le champ email doit être renseigné.'
        ]]);
    }

    /**
     * Test a registration without password
     *
     * @return void
     */
    public function testRegistrationWithoutPassword()
    {
        $this->createClient()->request('POST', '/api/v1/adopters', [
            'json' => [
                'email' => 'adopter+2@anidopt.bzh'
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'password: Le champ mot de passe doit être renseigné.'
        ]]);
    }

    /**
     * Test a failed registration with wrong email
     *
     * @return void
     */
    public function testRegistrationWithWrongEmail()
    {
        $this->createClient()->request('POST', '/api/v1/adopters', [
            'json' => [
                'email' => 'adopter+1anidopt.bzh',
                'password' => 'password'
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'email: La valeur renseigné n\'est pas un email valide.'
        ]]);
    }

    /**
     * Validate a JSON response from a POST user request
     *
     * @param Object $data
     * @param string $type
     * @param string $pathType
     * @return void
     */
    private function validateJsonResponse($data, $type, $pathType): void
    {
        $this->assertJsonContains(['@context' => '/api/v1/contexts/' . $type]);
        $this->assertJsonContains(['@id' => '/api/v1/' . $pathType . '/' . $data->uuid]);
        $this->assertJsonContains(['@type' => $type]);

        $this->assertJsonContains(['uuid' => $data->uuid]);
        $this->assertJsonContains(['email' => $data->email]);
        $this->assertJsonContains(['roles' => $data->roles]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
        $this->assertJsonContains(['account' => get_object_vars($data->account)]);
        $this->assertTrue($data->account->activationToken !== null);
        $this->assertTrue(!$data->account->isVerified);
    }
}
