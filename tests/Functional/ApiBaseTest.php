<?php

namespace App\Tests\Functional;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiBaseTest extends WebTestCase
{
    /**
     * Check if all is good for functional tests
     *
     * @return void
     */
    public function testApiBase(): void
    {
        $client = static::createClient();
        $client->request('GET', '/api/v1');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}
