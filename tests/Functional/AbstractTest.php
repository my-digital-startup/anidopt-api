<?php

namespace App\Tests\Functional;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

abstract class AbstractTest extends ApiTestCase
{
    private $token;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
    }

    /**
     * Create clien with credentials
     *
     * @param string $type
     * @param string|null $token
     * @return Client
     */
    protected function createClientWithCredentials($type = 'adopter', $token = null): Client
    {
        $token = $token ?: $this->getToken($type);

        return static::createClient([], ['headers' => ['authorization' => 'Bearer ' . $token]]);
    }

    /**
     * Create clien with credentials for PATCH method
     *
     * @param string $type
     * @param string|null $token
     * @return Client
     */
    protected function createClientWithCredentialsForPathMethod($type = 'adopter', $token = null): Client
    {
        $token = $token ?: $this->getToken($type);

        return static::createClient(
            [],
            [
                'headers' => [
                    'authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/merge-patch+json'
                ]
            ]
        );
    }

    /**
     * Get token
     *
     * @param string $type
     * @return string
     */
    protected function getToken($type = 'adopter'): string
    {
        if ($this->token) {
            return $this->token;
        }

        $response = $this->createClient()->request('POST', '/api/v1/login_check', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'username' => $type . '@anidopt.bzh',
                'password' => 'password'
            ],
        ]);

        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $this->token = $data->token;

        return $data->token;
    }
}
