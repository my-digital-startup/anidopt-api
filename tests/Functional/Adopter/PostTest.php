<?php

namespace App\Tests\Functional\Adopter;

use App\Entity\User\Adopter;
use App\Repository\User\AdopterRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class PostTest extends AbstractTest
{
    private EntityManager $entityManager;

    private AdopterRepository $adopterRepository;

    private Adopter $adopter;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->adopterRepository = $this->entityManager
            ->getRepository(Adopter::class);

        $this->adopter = $this->adopterRepository->findOneBy([]);
    }

    /**
     * POST /adopters
     *
     * @return void
     */
    public function testPostAdopter()
    {
        $response = $this->createClient()->request('POST', '/api/v1/adopters', [
            'json' => [
                'email' => 'adopter-test@anidopt.bzh',
                'password' => 'password'
            ]
        ]);

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(201);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Adopter']);
        $this->assertJsonContains(['@id' => '/api/v1/adopters/' . $data->uuid]);
        $this->assertJsonContains(['@type' => 'Adopter']);
        $this->assertJsonContains(['uuid' => $data->uuid]);
        $this->assertJsonContains(['email' => $data->email]);
        $this->assertJsonContains(['roles' => $data->roles]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
        $this->assertJsonContains(['account' => get_object_vars($data->account)]);
        $this->assertTrue($data->account->activationToken !== null);
        $this->assertTrue(!$data->account->isVerified);
    }

    /**
     * POST /adopters Without Email
     *
     * @return void
     */
    public function testPostAdopterWithoutEmail()
    {
        $this->createClient()->request('POST', '/api/v1/adopters', [
            'json' => [
                'password' => 'password'
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'email: Le champ email doit être renseigné.'
        ]]);
    }

    /**
     * POST /adopters Without Password
     *
     * @return void
     */
    public function testPostAdopterWithoutPassword()
    {
        $this->createClient()->request('POST', '/api/v1/adopters', [
            'json' => [
                'email' => 'adopter-test+1@anidopt.bzh',
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'password: Le champ mot de passe doit être renseigné.'
        ]]);
    }

    /**
     * POST /adopters Without Data
     *
     * @return void
     */
    public function testPostAdopterWithoutData()
    {
        $this->createClient()->request('POST', '/api/v1/adopters', ['json' => []]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'email: Le champ email doit être renseigné.',
            1 => 'password: Le champ mot de passe doit être renseigné.'
        ]]);
    }
}
