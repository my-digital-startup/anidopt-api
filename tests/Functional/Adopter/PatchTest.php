<?php

namespace App\Tests\Functional\Adopter;

use App\Entity\User\Adopter;
use App\Repository\User\AdopterRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class PatchTest extends AbstractTest
{
    private EntityManager $entityManager;

    private AdopterRepository $adopterRepository;

    private Adopter $adopter;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->adopterRepository = $this->entityManager
            ->getRepository(Adopter::class);

        $this->adopter = $this->adopterRepository->findOneBy([]);
    }

    /**
     * PATCH /adopters/{uuid}
     *
     * @return void
     */
    public function testPatchAdopter()
    {
        $response = $this->createClientWithCredentialsForPathMethod()->request(
            'PATCH',
            '/api/v1/adopters/' . $this->adopter->getUuid(),
            [
                'json' => [
                    'firstName' => 'Adopter firstName',
                    'lastName' => 'Adopter lastName',
                    'pseudo' => 'Adopter pseudo',
                    'account' => [
                        'address' => 'Adopter address',
                        'city' => 'Adopter city',
                        'zipCode' => "123456"
                    ]
                ],
            ]
        );

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['firstName' => $data->firstName]);
        $this->assertJsonContains(['lastName' => $data->lastName]);
        $this->assertJsonContains(['pseudo' => $data->pseudo]);
        $this->assertEquals('Adopter firstName', $data->firstName);
        $this->assertEquals('Adopter lastName', $data->lastName);
        $this->assertEquals('Adopter pseudo', $data->pseudo);
    }

    /**
     * PATCH /adopters/{uuid} Accès refusé.
     *
     * @return void
     */
    public function testPatchAdopterAccessDenied()
    {
        $adopter = $this->adopterRepository->findALl()[1];
        $this->createClientWithCredentialsForPathMethod()->request(
            'PATCH',
            '/api/v1/adopters/' . $adopter->getUuid(),
            [
                'json' => [
                    'firstName' => 'Adopter firstName',
                    'lastName' => 'Adopter lastName',
                    'pseudo' => 'Adopter pseudo',
                    'account' => [
                        'address' => 'Adopter address',
                        'city' => 'Adopter city',
                        'zipCode' => "123456"
                    ]
                ],
            ]
        );

        $this->assertResponseStatusCodeSame(403);
        $this->assertJsonContains(['status' => 403]);
        $this->assertJsonContains(['detail' => 'Accès refusé.']);
    }

    /**
     * PATCH /adopters/{uuid} Ressource non trouvée.
     *
     * @return void
     */
    public function testPatchAdopterNotFound()
    {
        $adopter = $this->adopterRepository->findAll()[1];
        $this->createClientWithCredentialsForPathMethod()->request(
            'PATCH',
            '/api/v1/adopters/' . $adopter->getUuid() . 'a',
            [
                'json' => [
                    'firstName' => 'Adopter firstName',
                    'lastName' => 'Adopter lastName',
                    'pseudo' => 'Adopter pseudo',
                    'account' => [
                        'address' => 'Adopter address',
                        'city' => 'Adopter city',
                        'zipCode' => "123456"
                    ]
                ],
            ]
        );

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
