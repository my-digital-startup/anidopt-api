<?php

namespace App\Tests\Functional\Adopter;

use App\Entity\User\Adopter;
use App\Repository\User\AdopterRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class GetTest extends AbstractTest
{
    private EntityManager $entityManager;

    private AdopterRepository $adopterRepository;

    private Adopter $adopter;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->adopterRepository = $this->entityManager
            ->getRepository(Adopter::class);

        $this->adopter = $this->adopterRepository->findOneBy([]);
    }

    /**
     * GET /adopters
     *
     * @return void
     */
    public function testGetAdopters()
    {
        $response = $this->createClient()->request('GET', '/api/v1/adopters');
        // dd($response);

        $data = json_decode($response->getContent());
        $hydraMember = $data->{'hydra:member'};
        $hydraTotalItems = $data->{'hydra:totalItems'};

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Adopter']);
        $this->assertJsonContains(['@id' => '/api/v1/adopters']);
        $this->assertJsonContains(['@type' => 'hydra:Collection']);
        $this->assertTrue($hydraMember !== null);
        $this->assertTrue($hydraTotalItems !== null);
    }

    /**
     * GET /adopters/{uuid}
     *
     * @return void
     */
    public function testGetAdopter()
    {
        $response = $this->createClientWithCredentials()->request('GET', '/api/v1/adopters/' . $this->adopter->getUuid());

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Adopter']);
        $this->assertJsonContains(['@id' => '/api/v1/adopters/' . $data->uuid]);
        $this->assertJsonContains(['@type' => 'Adopter']);
        $this->assertJsonContains(['uuid' => $data->uuid]);
        $this->assertJsonContains(['firstName' => $data->firstName]);
        $this->assertJsonContains(['lastName' => $data->lastName]);
        $this->assertJsonContains(['pseudo' => $data->pseudo]);
        $this->assertJsonContains(['email' => $data->email]);
        $this->assertJsonContains(['roles' => $data->roles]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
        $this->assertJsonContains(['account' => get_object_vars($data->account)]);
    }

    /**
     * GET /adopters/{uuid} Without be logged in
     *
     * @return void
     */
    public function testGetAdopterWithoutBeLoggedIn()
    {
        $this->createClient()->request('GET', '/api/v1/adopters/' . $this->adopter->getUuid());

        $this->assertResponseStatusCodeSame(401);
        $this->assertJsonContains(['status' => 401]);
        $this->assertJsonContains(['detail' => 'L\'élément nécéssaire à la connexion n\'a pas été trouvé. Veuillez réessayer.']);
    }

    /**
     * GET /adopters/{uuid} Accès refusé.
     *
     * @return void
     */
    public function testGetAdopterAccessDenied()
    {
        $adopter = $this->adopterRepository->findAll()[1];
        $this->createClientWithCredentials()->request('GET', '/api/v1/adopters/' . $adopter->getUuid());

        $this->assertResponseStatusCodeSame(403);
        $this->assertJsonContains(['status' => 403]);
        $this->assertJsonContains(['detail' => 'Accès refusé.']);
    }

    /**
     * GET /adopters/{uuid} Ressource non trouvée.
     *
     * @return void
     */
    public function testGetAdopterNotFound()
    {
        $this->createClientWithCredentials()->request('GET', '/api/v1/adopters/' . $this->adopter->getUuid() . 'a');

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }

    /**
     * GET /adopters/{uuid}/pets
     *
     * @return void
     */
    public function testGetAdopterlikedPets()
    {
        $response = $this->createClientWithCredentials()->request('GET', '/api/v1/adopters/' . $this->adopter->getUuid() . '/pets');

        $data = json_decode($response->getContent());
        $hydraMember = $data->{'hydra:member'};
        $hydraTotalItems = $data->{'hydra:totalItems'};

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Pet']);
        $this->assertJsonContains(['@id' => '/api/v1/adopters/' . $this->adopter->getUuid() . '/pets']);
        $this->assertJsonContains(['@type' => 'hydra:Collection']);
        $this->assertTrue($hydraMember !== null);
        $this->assertTrue($hydraTotalItems !== null);
    }

    /**
     * GET /adopters/{uuid}/shelters
     *
     * @return void
     */
    public function testGetAdopterLikedShelters()
    {
        $response = $this->createClientWithCredentials()->request('GET', '/api/v1/adopters/' . $this->adopter->getUuid() . '/shelters');

        $data = json_decode($response->getContent());
        $hydraMember = $data->{'hydra:member'};
        $hydraTotalItems = $data->{'hydra:totalItems'};

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Shelter']);
        $this->assertJsonContains(['@id' => '/api/v1/adopters/' . $this->adopter->getUuid() . '/shelters']);
        $this->assertJsonContains(['@type' => 'hydra:Collection']);
        $this->assertTrue($hydraMember !== null);
        $this->assertTrue($hydraTotalItems !== null);
    }
}
