<?php

namespace App\Tests\Functional\Breed;

use App\Tests\Functional\AbstractTest;

class PatchTest extends AbstractTest
{
    /**
     * PATCH /breeds/{id}
     *
     * @return void
     */
    public function testPatchBreed()
    {
        $response = $this->createClientWithCredentialsForPathMethod()->request('PATCH', '/api/v1/breeds/1', [
                'json' => [
                    'name' => 'BREED PATCH test'
                ],
            ]);

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Breed']);
        $this->assertJsonContains(['@id' => '/api/v1/breeds/1']);
        $this->assertJsonContains(['@type' => 'Breed']);
        $this->assertJsonContains(['id' => $data->id]);
        $this->assertJsonContains(['name' => $data->name]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
        $this->assertEquals('BREED PATCH test', $data->name);
    }

    /**
     * PATCH /breeds/{id} with an existing name
     *
     * @return void
     */
    public function testPatchBreedWithAnExistingName()
    {
        $this->createClientWithCredentialsForPathMethod()->request('PATCH', '/api/v1/breeds/2', [
                'json' => [
                    'name' => 'BREED PATCH test'
                ],
            ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'name: Cette valeur est déjà utilisée.'
        ]]);
    }

    /**
     * PATCH /breeds/{id} Ressource non trouvée.
     *
     * @return void
     */
    public function testPatchBreedNotFound()
    {
        $this->createClientWithCredentialsForPathMethod()->request('PATCH', '/api/v1/breeds/1000', [
            'json' => [
                'name' => 'BREED PATCH test 2'
            ],
        ]);

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
