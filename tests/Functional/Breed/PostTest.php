<?php

namespace App\Tests\Functional\Breed;

use App\Entity\Pet\Breed;
use App\Repository\Pet\BreedRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class PostTest extends AbstractTest
{
    private EntityManager $entityManager;

    private BreedRepository $breedRepository;

    private Breed $breed;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->breedRepository = $this->entityManager
            ->getRepository(Breed::class);

        $this->breed = $this->breedRepository->findOneBy([]);
    }

    /**
     * POST /breeds
     *
     * @return void
     */
    public function testPostBreed()
    {
        $response = $this->createClient()->request('POST', '/api/v1/breeds', [
            'json' => [
                'name' => 'BREED test'
            ]
        ]);

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(201);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Breed']);
        $this->assertJsonContains(['@id' => '/api/v1/breeds/' . $data->id]);
        $this->assertJsonContains(['@type' => 'Breed']);
        $this->assertJsonContains(['id' => $data->id]);
        $this->assertJsonContains(['name' => $data->name]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
    }

    /**
     * POST /breeds With existing name
     *
     * @return void
     */
    public function testPostBreedWithAnExistingName()
    {
        $this->createClient()->request('POST', '/api/v1/breeds', [
            'json' => [
                'name' => 'BREED test'
            ]
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'name: Cette valeur est déjà utilisée.',
        ]]);
    }

    /**
     * POST /breeds Without Data
     *
     * @return void
     */
    public function testPostBreedWithoutData()
    {
        $this->createClient()->request('POST', '/api/v1/breeds', ['json' => []]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains(['status' => 422]);
        $this->assertJsonContains(['detail' => [
            0 => 'name: Cette valeur ne doit pas être vide.',
        ]]);
    }
}
