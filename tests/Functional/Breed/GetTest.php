<?php

namespace App\Tests\Functional\Breed;

use App\Entity\Pet\Breed;
use App\Repository\Pet\BreedRepository;
use App\Tests\Functional\AbstractTest;
use Doctrine\ORM\EntityManager;

class GetTest extends AbstractTest
{
    private EntityManager $entityManager;

    private BreedRepository $breedRepository;

    private Breed $breed;

    /**
     * Prepare tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->breedRepository = $this->entityManager
            ->getRepository(Breed::class);

        $this->breed = $this->breedRepository->findOneBy([]);
    }

    /**
     * GET /breeds
     *
     * @return void
     */
    public function testGetBreeds()
    {
        $response = $this->createClient()->request('GET', '/api/v1/breeds');

        $data = json_decode($response->getContent());
        $hydraMember = $data->{'hydra:member'};
        $hydraTotalItems = $data->{'hydra:totalItems'};

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Breed']);
        $this->assertJsonContains(['@id' => '/api/v1/breeds']);
        $this->assertJsonContains(['@type' => 'hydra:Collection']);
        $this->assertTrue($hydraMember !== null);
        $this->assertTrue($hydraTotalItems !== null);
    }

    /**
     * GET /breeds/{uuid}
     *
     * @return void
     */
    public function testGetBreed()
    {
        $response = $this->createClientWithCredentials()->request('GET', '/api/v1/breeds/' . $this->breed->getId());

        $data = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
        $this->assertJsonContains(['@context' => '/api/v1/contexts/Breed']);
        $this->assertJsonContains(['@id' => '/api/v1/breeds/' . $data->id]);
        $this->assertJsonContains(['@type' => 'Breed']);
        $this->assertJsonContains(['name' => $data->name]);
        $this->assertJsonContains(['createdAt' => $data->createdAt]);
    }

    /**
     * GET /breeds/{uuid} Ressource non trouvée.
     *
     * @return void
     */
    public function testGetBreedNotFound()
    {
        $this->createClientWithCredentials()->request('GET', '/api/v1/breeds/' . ($this->breed->getId()) + 1000);

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
