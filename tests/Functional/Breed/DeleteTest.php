<?php

namespace App\Tests\Functional\Breed;

use App\Tests\Functional\AbstractTest;

class DeleteTest extends AbstractTest
{
    /**
     * DELETE /breeds/{id}
     *
     * @return void
     */
    public function testDeleteBreed()
    {
        $this->createClient()->request('DELETE', '/api/v1/breeds/3');
        $this->assertResponseStatusCodeSame(204);
    }

    /**
     * DELETE /breeds/{id} if breed not found
     *
     * @return void
     */
    public function testDeleteBreedNotFound()
    {
        $this->createClient()->request('DELETE', '/api/v1/breeds/3');

        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(['status' => 404]);
        $this->assertJsonContains(['detail' => 'Ressource non trouvée.']);
    }
}
