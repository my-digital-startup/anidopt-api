<!-- NAME -->

|                 |                                                                                 MASTER                                                                                 |                                                                                 DEVELOP                                                                                  |
| :-------------: | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
| PIPELINE STATUS | [![pipeline status](https://gitlab.com/my-digital-startup/anidopt-api/badges/master/pipeline.svg)](https://gitlab.com/my-digital-startup/anidopt-api/-/commits/master) | [![pipeline status](https://gitlab.com/my-digital-startup/anidopt-api/badges/develop/pipeline.svg)](https://gitlab.com/my-digital-startup/anidopt-api/-/commits/develop) |
| COVERAGE STATUS | [![coverage report](https://gitlab.com/my-digital-startup/anidopt-api/badges/master/coverage.svg)](https://gitlab.com/my-digital-startup/anidopt-api/-/commits/master) | [![coverage report](https://gitlab.com/my-digital-startup/anidopt-api/badges/develop/coverage.svg)](https://gitlab.com/my-digital-startup/anidopt-api/-/commits/develop) |

## Name

Anidopt API

<!-- DESCRIPTION -->

## Description

Anidopt will give abandoned animals visibility so that they can quickly find a new home. This project refers to the API, and is built using [Symfony](https://symfony.com/) and [API Platform](https://api-platform.com/).

<!-- CONTEXT -->

## Context

This application is a red thread project of our school year, for the subject: MyStartupProject, within the MyDigitalSchool. Anidopt is a platform that aims to put forward (by shelters) animals looking for a home, a family.

<!-- INSTALLATION -->

## Installation

Once the project is cloned on your machine, you will need to install [Lando](https://lando.dev/download/) and use this command:

    make init

This will allow you to install everything necessary for the proper functioning of the project.
If you have problems, your PC does not understand what the "make" command is, in this case, here are some things to explore: -[Windows](https://superuser.com/questions/808807/using-make-from-windows-powershell) -[Linux](https://askubuntu.com/questions/161104/how-do-i-install-make) -[MacOS](https://stackoverflow.com/questions/1469994/using-make-on-os-x)

<!-- USAGE -->

## Usage

You can start the application using the following command:

    make start

## Fixtures

To set up the fixtures, the following command must be used:

    make seed

Beforehand, you must create a folder called "Resources" in ./src/DataFixtures and fill it with images:

- bob.jpeg
- emilien.jpeg
- julien.jpeg
- alexis.jpeg

<!-- TESTS -->

## Tests

To run the tests, you will need to use the following command:

    make tests

Beforehand, you must create a folder called "Resources" in ./tests/Functional/MediaObject and fill it with files:

- big.jpg
- bob.jpeg
- file.txt

<!-- HELP -->

## Help

If you need (technical) help at project level, use this command:

    make help

Or contact the project manager at the following email address: emilien.gantois.pro@gmail.com if you don't find what you're looking for.

<!-- LICENSE -->

## License

[License]()

<!-- PROJECT STATUS -->

## Project Status

This project is under development and will be rated in the future.

<!-- DOCUMENTATION -->

## Documentation

[Repository](https://gitlab.com/my-digital-startup/anidopt-api-documentation)
