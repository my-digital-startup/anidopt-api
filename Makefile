LANDO 		= @lando
COMPOSER 	= $(LANDO) composer
PHP 			= $(LANDO) php
SYMFONY 	= $(PHP) bin/console

## —— 🔥 App —————————————————————————————————————————————————————————————————
init: ## Init the project
	$(MAKE) start-containers
	$(MAKE) install-vendor
	$(SYMFONY) lexik:jwt:generate-keypair
	$(MAKE) start

start: ## Start the app
	$(MAKE) stop-containers
	$(MAKE) start-containers

rebuild: ## Rebuild the app
	$(MAKE) clean-vendor
	$(LANDO) rebuild -y

cc: ## Clear the app's cache
	$(SYMFONY) cache:clear
	$(COMPOSER) cc
	$(LANDO) --clear

.PHONY: tests
tests: ## Run all tests
	$(SYMFONY) d:d:d --force --if-exists --env=test
	$(SYMFONY) d:d:c --env=test
	$(SYMFONY) d:m:m --no-interaction --env=test
	$(SYMFONY) d:f:l --no-interaction --env=test
	$(PHP) bin/phpunit --testdox tests/Unit/
	$(PHP) bin/phpunit --testdox tests/Functional/

analyze:
	-$(COMPOSER) valid
	-$(COMPOSER) diagnose
	-$(SYMFONY) doctrine:schema:valid --skip-sync


## —— 🐳 Lando ———————————————————————————————————————————————————————————————
start-containers: ## Starts all containers
	$(LANDO) start

stop-containers: ## Stop the containers
	$(LANDO) poweroff


## —— 🎻 Composer —————————————————————————————————————————————————————————————
install-vendor: ## Installs all vendor dependencies
	$(COMPOSER) install --prefer-dist

update-vendor: ## Updates all vendor dependencies
	$(COMPOSER) update

clean-vendor: ## Resets the vendor folder
	$(COMPOSER) cc
ifeq ($(OS), Linux)
	rm -Rf vendor
	rm composer.lock
	$(MAKE) install-vendor
endif
	$(MAKE) update-vendor

## —— 🎶 Symfony ——————————————————————————————————————————————————————————————
init-db: ## Init database
	$(SYMFONY) d:d:d --force --if-exists
	$(SYMFONY) d:d:c
	$(SYMFONY) d:m:m --no-interaction

seed: ## Fill database
	$(MAKE) init-db
	$(SYMFONY) d:f:l --no-interaction

entity: ## Set entity
	$(SYMFONY) make:entity

migration: ## Set migration
	$(SYMFONY) make:migration 

controller: ## Set controller
	$(SYMFONY) make:controller


## —— 🛠️  Others ————————————————————————————————————————————————————————————————
help: ## List of commands
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
